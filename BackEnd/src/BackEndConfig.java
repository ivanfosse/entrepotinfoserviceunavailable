import util.Config;

public class BackEndConfig {
	private String GeneralCertificateManagerRMIObjectUrl;
	private String ServerCertificateManagerRMIObjectUrl;
	private String WarehouseManagerRMIObjectUrl;
	private int HttpServerPort;
	
	public BackEndConfig() {
		Config config = new Config("./configs/WarehouseManagerConfig.json");
		GeneralCertificateManagerRMIObjectUrl = config.get("GeneralCertificateManagerRMIObjectUrl","rmi://localhost:28000/GeneralCertificateManager");
		ServerCertificateManagerRMIObjectUrl = config.get("ServerCertificateManagerRMIObjectUrl","rmi://localhost:28000/ServerCertificateManager");
		WarehouseManagerRMIObjectUrl = config.get("WarehouseManagerRMIObjectUrl","rmi://localhost:28000/WarehouseManager");
		HttpServerPort = Integer.parseInt(config.get("HttpServerPort","8081"));
		}


	public String getGeneralCertificateManagerRMIObjectUrl() {
		return GeneralCertificateManagerRMIObjectUrl;
	}

	public String getServerCertificateManagerRMIObjectUrl() {
		return ServerCertificateManagerRMIObjectUrl;
	}

	public String getWarehouseManagerRMIObjectUrl() {
		return WarehouseManagerRMIObjectUrl;
	}
	
	public int getHttpServerPort() {
		return HttpServerPort;
	}
	
	
}
