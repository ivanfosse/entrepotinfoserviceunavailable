import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;

import org.json.JSONException;
import org.json.JSONObject;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import rmiIterface.RmiWarehouseManager;
import util.VerifyCertificate;
import util.exception.BadCertificateException;
import util.exception.CertificateExpiredException;

public class BackEndCommand implements HttpHandler {
	private BackEndConfig backEndConfig;
	private VerifyCertificate verifyCertificate;
	private String serverCertificate;
	private RmiWarehouseManager rmiWarehouseManager;

	public BackEndCommand(BackEndConfig backEndConfig, VerifyCertificate verifyCertificate, String serverCertificate,
			RmiWarehouseManager rmiWarehouseManager) {
		super();
		this.backEndConfig = backEndConfig;
		this.verifyCertificate = verifyCertificate;
		this.serverCertificate = serverCertificate;
		this.rmiWarehouseManager = rmiWarehouseManager;
	}


	@Override
	public void handle(HttpExchange t) throws IOException {
		System.out.println(t.getRemoteAddress());
		boolean sendForm = false;
    	String userCertificate = null;
    	String command = null;
    	String answer = "";
		BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(t.getRequestBody(),"utf-8"));
        } catch(UnsupportedEncodingException e) {
            System.err.println("Erreur lors de la r�cup�ration du flux " + e);
            System.exit(-1);
        }
        String query = null;
        try {
            query = br.readLine();
        } catch(IOException e) {
            System.err.println("Erreur lors de la lecture d'une ligne " + e);
            System.exit(-1);
        }
        if(query != null) {
        	String[] args = query.split("&");
        	for (int i = 0; i < args.length; i++) {
				String[] q2 = args[i].split("=");
				if(q2.length < 2)
					continue;
				if(URLDecoder.decode(q2[0],"ISO-8859-1").equals("usercertificate"))
					userCertificate = URLDecoder.decode(q2[1],"ISO-8859-1");
				if(URLDecoder.decode(q2[0],"ISO-8859-1").equals("command"))
					command = URLDecoder.decode(q2[1],"ISO-8859-1");
			}
        	System.out.println(userCertificate);
        	System.out.println(command);
        	if(userCertificate != null && !userCertificate.equals("") && command != null && !command.equals(""))
        		sendForm = true;
        }
    	System.out.println(sendForm);
        if(sendForm) {
        	try {
				verifyCertificate.verifyCertificate(userCertificate,"User");
        		JSONObject k = new JSONObject(new JSONObject(userCertificate).getString("Certificate"));
        		JSONObject o = new JSONObject(command);
        		o.put("UserOwnerId", k.getLong("GroupId"));
        		System.out.println(o.toString());
	        	answer = rmiWarehouseManager.executeCommand(serverCertificate, o.toString());
	        	System.out.println(answer);
	        	if(answer == null)
	        		sendForm = false;
			} catch (IllegalBlockSizeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				JSONObject j = new JSONObject();
				j.put("Answer", "Error : Not a Certificate");
				answer = j.toString();
			} catch (BadPaddingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				JSONObject j = new JSONObject();
				j.put("Answer", "Error : Not a Certificate");
				answer = j.toString();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				JSONObject j = new JSONObject();
				j.put("Answer", "Error : Not a Certificate");
				answer = j.toString();
			} catch (BadCertificateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				JSONObject j = new JSONObject();
				j.put("Answer", "Error : Bas Certificate");
				answer = j.toString();
			} catch (CertificateExpiredException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				JSONObject j = new JSONObject();
				j.put("Answer", "Error : User Certificate Expired");
				answer = j.toString();
			}
        }
        if(!sendForm) {
		    try {
		        Headers h = t.getResponseHeaders();
		        h.set("Content-Type", "text/html; charset=utf-8");
		        t.sendResponseHeaders(404, 0);
		    } catch(IOException e) {
		        System.err.println("Erreur lors de l'envoi de l'en-t�te : " + e);
		        System.exit(-1);
		    }
		    try {
		        OutputStream os = t.getResponseBody();
		        os.close();
		    } catch(IOException e) {
		        System.err.println("Erreur lors de l'envoi du corps : " + e);
		    }
        }else {
        	String ans = answer;
		    try {
		        Headers h = t.getResponseHeaders();
		        h.set("Content-Type", "application/json; charset=utf-8");
		        t.sendResponseHeaders(200, ans.getBytes("UTF-8").length);
		    } catch(IOException e) {
		        System.err.println("Erreur lors de l'envoi de l'en-t�te : " + e);
		        System.exit(-1);
		    }
		    try {
		        OutputStream os = t.getResponseBody();
		        os.write(ans.getBytes("UTF-8"));
		        os.close();
		    } catch(IOException e) {
		        System.err.println("Erreur lors de l'envoi du corps : " + e);
		    }
        }
	}
}
