<?php 

	include("Plan.php");

	define("COMMAND_URL", "http://localhost:8081/command.jeej"); 

    if(!isset($_FILES["usercertificate"]) || !isset($_POST["warehousename"])){
        echo "Need usercertificate and warehousename";
        exit(1);
    }

    $usercertificate = file_get_contents($_FILES['usercertificate']['tmp_name']);
    $warehousename = $_POST["warehousename"];

	
	//var_dump($_POST);

	$tableau = array("usercertificate" => $usercertificate, "command" => '{
   "Type": "WarehouseInfo",
   "Warehouse": "'.$warehousename.'"
}');

    $query = http_build_query($tableau);
    $ch    = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_URL, COMMAND_URL);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
    $response = curl_exec($ch);
    curl_close($ch);

    // Analyse du JSON reçu
    $tableau = json_decode($response, true);
    
    //var_dump($response);
    //var_dump($tableau);

    if(!isset($tableau) || !isset($tableau["Values"])){
    	header("Content-type: application/json");
    	echo "Error getting info ";
        echo $tableau["Answer"];
    	exit(0);
    }

    $donnees = $tableau["Values"];

    $max = $tableau["Height"];
    for($i = 0; $i < $tableau["Row"]; $i++) {
        for($j = 0; $j < $tableau["Column"]; $j++) {
        	if($donnees[$i][$j] == $max){
				$donnees[$i][$j] = 2;
        	}else if($donnees[$i][$j] != 0){
				$donnees[$i][$j] = 1;
        	}
        }
    }


	$plan = new Plan(300, 300, $tableau["Row"], $tableau["Column"]);                          // Nombre de places par rangée

    //var_dump($donnees);

    header("Content-type: image/png");
	imagepng($plan->getImage($donnees));
	exit();








































 ?>