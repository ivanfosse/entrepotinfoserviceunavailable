

import util.Config;

public class RmiRegistryServerConfig {
	private String ipAdress;
	private String port;
	
	public RmiRegistryServerConfig(){
		Config config = new Config("./configs/RMIRegistryConfig.json");
		ipAdress = config.get("RegistryIpAdress", "localhost");
		port = config.get("RegistryPort", "28000");
	}

	public String getIpAdress() {
		return ipAdress;
	}

	public String getPort() {
		return port;
	}
	
	public String getUrl() {
		return "rmi://"+ipAdress+ ":" + port+"/";
	}
}
