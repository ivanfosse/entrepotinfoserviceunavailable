import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import util.Command;
import util.Console;
import util.Logger;

public class RmiRegistryServer {
	RmiRegistryServerConfig rrl;
	Registry registry;
	Console console;
	
	public RmiRegistryServer() {
		rrl = new RmiRegistryServerConfig();
		if(rrl.getIpAdress().equals("localhost")) {
			System.out.println("Lancement du serveur de registre local.");
			try {
				registry = LocateRegistry.createRegistry(Integer.parseInt(rrl.getPort()));
			} catch (RemoteException e) {
				System.out.println("Impossible de cr�er le registre local.");
				e.printStackTrace();
			}
			System.out.println("Serveur local lanc�.");
			initConsole();
		}else {
			System.out.println("V�rification de l'accessibilit� du serveur.");
			System.out.println("Server localis� et accessible.");
			try {
				Registry registry = LocateRegistry.getRegistry(rrl.getIpAdress(), Integer.parseInt(rrl.getPort()));
			} catch (RemoteException e) {
				System.out.println("Impossible d'acc�der au serveur.");
				e.printStackTrace();
			}
		}
	}
	
	private void initConsole(){
		console = new Console();
		console.addCommand("list", new Command() {
			@Override
			public void execute(String[] args) {
				try {
					System.out.println("Listage des objets distant.");
					for (int i = 0; i < registry.list().length; i++) {
						System.out.println(registry.list()[i]);
					}
				} catch (RemoteException e) {
					System.out.println("Impossible de lister les objets distant.");
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		console.addCommand("unbind", new Command() {
			@Override
			public void execute(String[] args) {
				try {
					registry.unbind(args[1]);
				} catch (RemoteException | NotBoundException e) {
					System.out.println("Impossible de unbind :"+ args[1]);
					e.printStackTrace();
				}
			}
		});
		console.launch();
		System.exit(0);
	}
	
	public static void main(String[] args) throws InterruptedException, RemoteException {
		Logger.initialiseStream();
		RmiRegistryServer rrs = new RmiRegistryServer();
	}
}
