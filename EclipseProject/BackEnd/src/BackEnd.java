import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.json.JSONException;

import com.sun.net.httpserver.HttpServer;

import rmiIterface.RmiGeneralCertificateManager;
import rmiIterface.RmiServerCertificateManager;
import rmiIterface.RmiWarehouseManager;
import util.Console;
import util.Logger;
import util.VerifyCertificate;
import util.exception.BadCertificateException;
import util.exception.CertificateExpiredException;
import util.exception.WrongCertificateException;

public class BackEnd{
	private static final long serialVersionUID = 1753260919653073652L;
	private BackEndConfig backEndConfig;
	private Console console;
	private HttpServer httpServer;
	private BackEndCommand backEndCommand;
	
	private String generalCertificate;
	private VerifyCertificate verifyCertificate;
	private String serverCertificate;
	private int nextPortAlloc;

	public BackEnd() throws RemoteException{
		backEndConfig = new BackEndConfig();
		
		System.out.println("Getting General Certificate");
		try {
			RmiGeneralCertificateManager rmiGeneralCertificateManager = (RmiGeneralCertificateManager)Naming.lookup(backEndConfig.getGeneralCertificateManagerRMIObjectUrl());
			generalCertificate = rmiGeneralCertificateManager.getGeneralCertificate();
			verifyCertificate = new VerifyCertificate(rmiGeneralCertificateManager.getGeneralCertificate());
		} catch (MalformedURLException | RemoteException | NotBoundException | InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException | JSONException | IllegalBlockSizeException | BadPaddingException | WrongCertificateException | CertificateExpiredException | BadCertificateException e) {
			System.err.println("Cannot get general certificate shuting down");
			e.printStackTrace();
			System.exit(1);
		}
		System.out.println("Getting Server Certificate");
		try {
			serverCertificate = ((RmiServerCertificateManager)Naming.lookup(backEndConfig.getServerCertificateManagerRMIObjectUrl())).getServerCertificate("BackEnd");
			verifyCertificate.verifyCertificate(serverCertificate, "Server");
		} catch (IllegalBlockSizeException | BadPaddingException | JSONException 
				| BadCertificateException | CertificateExpiredException | MalformedURLException | NotBoundException e) {
			System.err.println("Cannot get server certificate shuting down");
			e.printStackTrace();
			System.exit(1);
		}
		try {
			httpServer = HttpServer.create(new InetSocketAddress(backEndConfig.getHttpServerPort()), 0);
        } catch(IOException e) {
            System.err.println("Erreur lors de la cr�ation du serveur " + e);
            System.exit(-1);
        }
		try {
			backEndCommand = new BackEndCommand(backEndConfig, verifyCertificate, serverCertificate, (RmiWarehouseManager)Naming.lookup(backEndConfig.getWarehouseManagerRMIObjectUrl()));
		} catch (MalformedURLException | NotBoundException e) {
            System.err.println("Erreur lors de la cr�ation du serveur " + e);
            System.exit(-1);
		}
		
		httpServer.createContext("/command.jeej", backEndCommand);
		httpServer.setExecutor(null);
		httpServer.start();
		
		
		initConsole();
	}
	
	
	private void initConsole(){
		console = new Console();
		
		console.launch();
		System.exit(0);
	}
	
	
	
	
	public static void main(String[] args) throws RemoteException {
		Logger.initialiseStream();
		BackEnd auth = new BackEnd();
	}
}
