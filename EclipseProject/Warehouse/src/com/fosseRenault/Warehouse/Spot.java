/**
 * 
 */
package com.fosseRenault.Warehouse;

import java.util.Objects;

import org.json.JSONObject;

/**
 *
 *
 * @author Jeremie Renault
 * @version 0.1 2017-12-01
 *
 */
public class Spot
{
	private int	row;
	private int	column;
	private int	height;

	/**
	 * Constructeur permettant d'initialiser une nouvelle instance de Spot.java.
	 *
	 */
	public Spot( int row,
				 int column,
				 int height )
	{
		this.row = row;
		this.column = column;
		this.height = height;
	}

	public Spot( String string )
	{
		this( Integer.parseInt( string.split( "-" )[0] ), Integer.parseInt( string.split( "-" )[1] ),Integer.parseInt( string.split( "-" )[2] ) );
	}

	public Spot( Spot spot )
	{
		this.row = spot.row;
		this.column = spot.column;
		this.height = spot.height;
	}

	public Spot( JSONObject jsonObject )
	{
		this( jsonObject.getInt( "row" ), jsonObject.getInt( "column" ), jsonObject.getInt( "height" ) );
	}

	/**
	 * Permet d'obtenir la valeur de row.
	 *
	 * @return the row
	 */
	public int getRow()
	{
		return row;
	}

	/**
	 * Permet d'obtenir la valeur de column.
	 *
	 * @return the column
	 */
	public int getColumn()
	{
		return column;
	}

	/**
	 * Permet d'obtenir la valeur de height.
	 *
	 * @return the height
	 */
	public int getHeight()
	{
		return height;
	}

	/**
	 * Permet de définir la valeur de row
	 *
	 * @param row
	 *        Le row.
	 */
	public void setRow( int row )
	{
		this.row = row;
	}

	/**
	 * Permet de définir la valeur de column
	 *
	 * @param column
	 *        Le column.
	 */
	public void setColumn( int column )
	{
		this.column = column;
	}

	/**
	 * Permet de définir la valeur de height
	 *
	 * @param height
	 *        Le height.
	 */
	public void setHeight( int height )
	{
		this.height = height;
	}

	public JSONObject toJSON()
	{
		return new JSONObject( this );
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return String.format( "%d-%d-%d", row, column, height );
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		return Objects.hash( this.row, this.column, this.height );
	}

	@Override
	public boolean equals( Object obj )
	{
		Spot spot = ( Spot ) obj;

		return this.row == spot.row && this.column == spot.column && this.height == spot.height;
	}

	public static void main( String[] args )
	{
		Spot s1 = new Spot( 0, 0, 0 );
		Spot s2 = new Spot( 0, 0, 0 );
		Spot s3 = new Spot( 1, 0, 0 );
		Spot s4 = new Spot( 0, 1, 0 );
		Spot s5 = new Spot( 0, 0, 1 );
		Spot s6 = new Spot( s1 );

		Spot[] arrS = { s1, s2, s3, s4, s5, s6 };

		for( int i = 0; i < arrS.length; i++ )
		{
			System.out.println( arrS[i] );
		}
	}
}
