package com.fosseRenault.Warehouse;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.nio.charset.Charset;

public abstract class ServerUdp {
	private int port;
	private DatagramSocket datagramSocketReceive;
	private byte[] tampon;
	private DatagramPacket datagramPacket;
	private boolean stop = false;
	private Charset charset = null;
	

	{
		if (Charset.isSupported("UTF-8"))
			charset = Charset.forName("UTF-8");
		else {
			charset = Charset.defaultCharset();
			System.out.println("Unsuported UTF-8 charged default charset :" + charset.name());
		}
	}

	public ServerUdp(int port, int tamponSize) {
		this.port = port;
		tampon = new byte[tamponSize];
		datagramPacket = new DatagramPacket(tampon, tamponSize);
	}

	public void launch() throws SocketException {
		stop = false;
		datagramSocketReceive = new DatagramSocket(port);
		while (!stop) {
			try {
				try {
					synchronized (datagramSocketReceive) {
						datagramSocketReceive.setSoTimeout(100);
						datagramSocketReceive.receive(datagramPacket);
					}
					this.receivePacket(new String(datagramPacket.getData(), 0, datagramPacket.getLength(), charset), datagramPacket.getAddress(), datagramPacket.getPort());
				}catch(SocketTimeoutException e) {
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		datagramSocketReceive.close();
		datagramSocketReceive = null;
	}

	public abstract void receivePacket(String string, InetAddress adress, int port);
	
	public synchronized void sendPacket(String string, InetAddress adress, int port) throws IOException {
		synchronized (datagramSocketReceive) {
			byte[] b = string.getBytes(charset);
			DatagramPacket datagramPacket = new DatagramPacket(b, b.length);
			datagramPacket.setAddress(adress);
			datagramPacket.setPort(port);
			datagramSocketReceive.send(datagramPacket);
		}
	}

	/**
	 * R�cup�re le charset utilis� pour encrypter les donn�es.
	 */
	public Charset getCharset() {
		return charset;
	}

	/**
	 * D�finie le charset utilis� pour encrypter les donn�es.
	 * 
	 * @param charset
	 *            Le charset
	 */
	public void setCharset(Charset charset) {
		this.charset = charset;
	}

}