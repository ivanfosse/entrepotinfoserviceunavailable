/**
 * 
 */
package com.fosseRenault.Warehouse.Container;

import java.util.ArrayList;

import org.json.JSONObject;

import com.fosseRenault.Warehouse.Owner;
import com.fosseRenault.Warehouse.Stage;
import com.fosseRenault.Warehouse.Warehouse;

/**
 *
 *
 * @author Jeremie Renault
 * @version 0.1 2017-12-14
 *
 */
public class LooseContainer extends Container
{
	private String looseType;

	/**
	 * Constructeur permettant d'initialiser une nouvelle instance de LooseContainer.java.
	 *
	 * @param ID
	 * @param owner
	 * @param weight
	 * @param roadmap
	 * @param looseType
	 */
	protected LooseContainer( String ID,
							  Owner owner,
							  double weight,
							  ArrayList< Stage > roadmap,
							  String looseType )
	{
		super( ID, owner, weight, roadmap );
		this.looseType = looseType;
	}

	public LooseContainer( int ID,
						   Owner owner,
						   double weight,
						   ArrayList< Stage > roadmap,
						   String looseType )
	{
		super( String.format( "LC%010d", ID ), owner, weight, roadmap );
		this.looseType = looseType;
	}

	/**
	 * Constructeur permettant d'initialiser une nouvelle instance de LooseContainer.java.
	 *
	 */
	public LooseContainer( int ID,
						   Owner owner, String looseType)
	{
		this( ID, owner, 0, null, looseType );
	}
	
	/**
	 * Constructeur permettant d'initialiser une nouvelle instance de LooseContainer.java.
	 *
	 */
	public LooseContainer( JSONObject jsonObject, Warehouse warehouse )
	{
		super( jsonObject, warehouse );
		this.looseType = jsonObject.getString( "looseType" );
		fill( ( float ) jsonObject.getDouble( "weight" ) );
	}
	
	public void fill( float weight )
	{
		addWeight( weight );
	}
	
	public void empty( float weight )
	{
		removeWeight( weight );
	}

	/**
	 * Permet d'obtenir la valeur de looseType.
	 *
	 * @return the looseType
	 */
	public String getLooseType()
	{
		return looseType;
	}
	
	@Override
	public void update(JSONObject jsonObject, Warehouse wh) {
		super.update(jsonObject, wh);
		if(jsonObject.has("looseType"))
			this.looseType = jsonObject.getString( "looseType" );
		removeWeight((float)super.getWeight());
		if(jsonObject.has("weight"))
			fill( ( float ) jsonObject.getDouble( "weight" ) );
	}

	/**
	 * Permet de définir la valeur de looseType
	 *
	 * @param looseType
	 *        Le looseType.
	 */
	public void setLooseType( String looseType )
	{
		this.looseType = looseType;
	}

	public JSONObject toJSON()
	{
		JSONObject j = new JSONObject( this );
		j.put("Type","LooseContainer");
		return j;
	}
}
