/**
 * 
 */
package com.fosseRenault.Warehouse.Container;

import java.util.ArrayList;

import org.json.JSONObject;

import com.fosseRenault.Warehouse.Owner;
import com.fosseRenault.Warehouse.Stage;
import com.fosseRenault.Warehouse.Warehouse;

/**
 *
 *
 * @author Jeremie Renault
 * @version 0.1 2017-12-14
 *
 */
public class FluidContainer extends Container {
	private String fluidType;
	private float density;
	private double volume;

	/**
	 * Constructeur permettant d'initialiser une nouvelle instance de
	 * FluidContainer.java.
	 *
	 * @param ID
	 * @param owner
	 * @param weight
	 * @param roadmap
	 * @param fluidType
	 * @param density
	 * @param volume
	 */
	protected FluidContainer(String ID, Owner owner, ArrayList<Stage> roadmap, String fluidType, float density,
			double volume) {
		super(ID, owner, density * volume, roadmap);
		this.fluidType = fluidType;
		this.density = density;
		this.volume = volume;
	}

	public FluidContainer(int ID, Owner owner, ArrayList<Stage> roadmap, String fluidType, float density,
			double volume) {
		super(String.format("FC%010d", ID), owner, density * volume, new ArrayList<>());
		this.fluidType = fluidType;
		this.density = density;
		this.volume = volume;
	}

	/**
	 * Constructeur permettant d'initialiser une nouvelle instance de
	 * FluidContainer.java.
	 *
	 */
	public FluidContainer(int ID, Owner owner, String fluidType, float density, double volume) {
		this(ID, owner, null, fluidType, density, volume);
	}

	/**
	 * Constructeur permettant d'initialiser une nouvelle instance de
	 * FluidContainer.java.
	 *
	 */
	public FluidContainer(JSONObject jsonObject, Warehouse warehouse) {
		super(jsonObject, warehouse);

		this.fluidType = jsonObject.getString("fluidType");
		this.density = (float) jsonObject.getDouble("density");
		this.volume = 0;
		fill((float) jsonObject.getDouble("volume"));
	}

	public void changeFluid(String fluidType, float density) {
		setFluidType(fluidType);
		setDensity(density);
	}

	public void fill(float volume) {
		addWeight((float) (volume * this.density));
		this.volume += volume;
	}

	public void empty(float volume) {
		if (volume > this.volume)
			throw new IllegalArgumentException("Le volume que vous essayez d'enlever est supérieur au volume contenu");

		this.volume -= volume;
		removeWeight(volume * density);
	}

	/**
	 * Permet d'obtenir la valeur de fluidType.
	 *
	 * @return the fluidType
	 */
	public String getFluidType() {
		return fluidType;
	}

	/**
	 * Permet d'obtenir la valeur de density.
	 *
	 * @return the density
	 */
	public float getDensity() {
		return density;
	}

	/**
	 * Permet d'obtenir la valeur de volume.
	 *
	 * @return the volume
	 */
	public double getVolume() {
		return volume;
	}

	/**
	 * Permet de définir la valeur de fluidType
	 *
	 * @param fluidType
	 *            Le fluidType.
	 */
	public void setFluidType(String fluidType) {
		this.fluidType = fluidType;
	}

	/**
	 * Permet de définir la valeur de density
	 *
	 * @param density
	 *            Le density.
	 */
	public void setDensity(float density) {
		this.density = density;
	}
	
	@Override
	public void update(JSONObject jsonObject, Warehouse wh) {
		super.update(jsonObject, wh);
		this.fluidType = jsonObject.getString("fluidType");
		this.density = (float) jsonObject.getDouble("density");
		this.volume = 0;
		removeWeight((float)super.getWeight());
		fill((float) jsonObject.getDouble("volume"));
	}

	/**
	 * Permet de définir la valeur de volume
	 *
	 * @param volume
	 *            Le volume.
	 */
	public void setVolume(double volume) {
		this.volume = volume;
	}

	public JSONObject toJSON()
	{
		JSONObject j = new JSONObject( this );
		j.put("Type","FluidContainer");
		return j;
	}
}
