/**
 * 
 */
package com.fosseRenault.Warehouse.Container;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import org.json.JSONObject;

import com.fosseRenault.Warehouse.Owner;
import com.fosseRenault.Warehouse.Stage;
import com.fosseRenault.Warehouse.Warehouse;

/**
 *
 *
 * @author Jeremie Renault
 * @version 0.1 2017-12-01
 *
 */
public class SimpleContainer extends Container
{
	private HashMap< String, Float > merchandises;

	protected SimpleContainer( String ID,
							   Owner owner,
							   ArrayList< Stage > roadmap,
							   HashMap< String, Float > merchandises )
	{
		super( ID, owner, 0, roadmap );
		if( merchandises != null )
			this.merchandises = new HashMap< String, Float >( merchandises );
		else
			this.merchandises = new HashMap< String, Float >();
		double w = 0;
		for (Entry<String, Float> iterable_element :  merchandises.entrySet()) {
			w += iterable_element.getValue();
		}
	}

	public SimpleContainer( long ID,
							Owner owner,
							ArrayList< Stage > roadmap,
							HashMap< String, Float > merchandises )
	{
		this( String.format( "SC%010d", ID ), owner, roadmap, merchandises );
	}

	public SimpleContainer( long ID,
							Owner owner,
							HashMap< String, Float > merchandises)
	{
		this( String.format( "SC%010d", ID ), owner, null, merchandises );
		
	}

	public SimpleContainer( JSONObject jsonObject,
							Warehouse warehouse )
	{
		super( jsonObject, warehouse );
		this.merchandises = new HashMap< String, Float >();

		JSONObject jo = jsonObject.getJSONObject( "merchandises" );

		for( String name : jo.keySet() )
		{
			fill( name, ( float ) jo.getDouble( name ) );
		}
	}

	public void fill( String label,
					  float weight )
	{
		addWeight( weight );
		String ucLbl = label.toUpperCase();
		merchandises.put( ucLbl, weight );
	}

	public void empty( String label,
					   float weight )
	{
		String ucLbl = label.toUpperCase();

		if( !merchandises.containsKey( ucLbl ) )
			throw new IllegalArgumentException( "La conteneur ne contient pas de " + label );

		float mercWeight = merchandises.get( ucLbl );

		if( weight > mercWeight )
			throw new IllegalArgumentException(
					"Le poind que vous essayez d'enlever est supérieur au poids de " + label + " contenu (" + mercWeight + "kg)" );

		mercWeight -= weight;

		merchandises.replace( ucLbl, mercWeight );
		removeWeight( weight );
	}

	/**
	 * Permet d'obtenir la valeur de merchandises.
	 *
	 * @return the merchandises
	 */
	public HashMap< String, Float > getMerchandises()
	{
		return merchandises;
	}

	/**
	 * Permet de définir la valeur de merchandises
	 *
	 * @param merchandises
	 *        Le merchandises.
	 */
	public void setMerchandises( HashMap< String, Float > merchandises )
	{
		this.merchandises = merchandises;
	}

	public JSONObject toJSON()
	{
		JSONObject j = new JSONObject( this );
		j.put("Type","SimpleContainer");
		return j;
	}
	
	@Override
	public void update(JSONObject jsonObject, Warehouse wh) {
		super.update(jsonObject, wh);
		super.removeWeight((float)this.getWeight());
		this.merchandises = new HashMap< String, Float >();

		JSONObject jo = jsonObject.getJSONObject( "merchandises" );

		for( String name : jo.keySet() )
		{
			fill( name, ( float ) jo.getDouble( name ) );
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "SimpleContainer [merchandises=" + merchandises + ", toString()=" + super.toString() + "]";
	}
}
