/**
 * 
 */
package com.fosseRenault.Warehouse.Container;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONObject;

import com.fosseRenault.Warehouse.Owner;
import com.fosseRenault.Warehouse.Stage;
import com.fosseRenault.Warehouse.Warehouse;

/**
 *
 *
 * @author Jeremie Renault
 * @version 0.1 2017-12-01
 *
 */
public class RefrigeratedContainer extends SimpleContainer
{
	private float		  temperature;
	private long		  workingDuration; //temps en secondes.
	private LocalDateTime startTime;	   //Timestamp

	/**
	 * Constructeur permettant d'initialiser une nouvelle instance de RefrigeratedContainer.java.
	 *
	 * @param ID
	 * @param owner
	 * @param weight
	 * @param roadmap
	 * @param merchandises
	 * @param temperature
	 * @param workingDuration
	 * @param startTime
	 */
	protected RefrigeratedContainer( String ID,
									 Owner owner,
									 ArrayList< Stage > roadmap,
									 HashMap< String, Float > merchandises,
									 float temperature,
									 long workingDuration,
									 LocalDateTime startTime )
	{
		super( ID, owner, roadmap, merchandises );
		this.temperature = temperature;
		this.workingDuration = workingDuration;
		this.startTime = startTime;
	}

	public RefrigeratedContainer( int ID,
								  Owner owner,
								  ArrayList< Stage > roadmap,
								  HashMap< String, Float > merchandises,
								  float temperature,
								  long workingDuration,
								  LocalDateTime startTime )
	{
		this( String.format( "RC%010d", ID ), owner,  roadmap, merchandises, temperature, workingDuration, startTime );
	}

	public RefrigeratedContainer( int ID,
								  Owner owner, float temperature,long workingDuration, LocalDateTime startTime, HashMap< String, Float > merchandises)
	{
		this( ID, owner, null, merchandises,temperature, workingDuration, startTime );
	}

	public RefrigeratedContainer( JSONObject jsonObject,
								  Warehouse warehouse )
	{
		super( jsonObject, warehouse );
		this.temperature = ( float ) jsonObject.getDouble( "temperature" );
		this.workingDuration = jsonObject.getLong( "workingDuration" );
		this.startTime = LocalDateTime.parse( jsonObject.getString( "startTime" ) );
	}

	public void start( float temperature,
					   long workingDuration )
	{
		this.temperature = temperature;
		this.workingDuration = workingDuration;
		this.startTime = LocalDateTime.now();
	}

	/**
	 * Permet d'obtenir la valeur de temperature.
	 *
	 * @return the temperature
	 */
	public float getTemperature()
	{
		return temperature;
	}

	/**
	 * Permet d'obtenir la valeur de workingDuration.
	 *
	 * @return the workingDuration
	 */
	public long getWorkingDuration()
	{
		return workingDuration;
	}

	/**
	 * Permet d'obtenir la valeur de startTime.
	 *
	 * @return the startTime
	 */
	public LocalDateTime getStartTime()
	{
		return startTime;
	}

	/**
	 * Permet de définir la valeur de temperature
	 *
	 * @param temperature
	 *        Le temperature.
	 */
	public void setTemperature( float temperature )
	{
		this.temperature = temperature;
	}

	/**
	 * Permet de définir la valeur de workingDuration
	 *
	 * @param workingDuration
	 *        Le workingDuration.
	 */
	public void setWorkingDuration( int workingDuration )
	{
		this.workingDuration = workingDuration;
	}
	
	@Override
	public void update(JSONObject jsonObject, Warehouse wh) {
		super.update(jsonObject, wh);
		this.temperature = ( float ) jsonObject.getDouble( "temperature" );
		this.workingDuration = jsonObject.getLong( "workingDuration" );
		this.startTime = LocalDateTime.parse( jsonObject.getString( "startTime" ) );
	}

	/**
	 * Permet de définir la valeur de startTime
	 *
	 * @param startTime
	 *        Le startTime.
	 */
	public void setStartTime( LocalDateTime startTime )
	{
		this.startTime = startTime;
	}

	public JSONObject toJSON()
	{
		JSONObject j = new JSONObject( this );
		j.put("Type","RefrigeratedContainer");
		return j;
	}
}
