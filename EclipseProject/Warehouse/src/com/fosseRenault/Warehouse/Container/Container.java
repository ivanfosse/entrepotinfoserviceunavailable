/**
 * 
 */
package com.fosseRenault.Warehouse.Container;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.fosseRenault.Warehouse.Owner;
import com.fosseRenault.Warehouse.Stage;
import com.fosseRenault.Warehouse.Warehouse;

/**
 *
 *
 * @author Jeremie Renault
 * @version 0.1 2017-12-01
 *
 */
public abstract class Container
{
	private final String	   ID;
	private Owner			   owner;
	private double			   weight;
	private ArrayList< Stage > roadmap;

	/**
	 * Constructeur permettant d'initialiser une nouvelle instance de Containers.java.
	 *
	 */
	public Container( String ID,
					  Owner owner,
					  double weight,
					  ArrayList< Stage > roadmap )
	{
		this.ID = ID;
		this.owner = owner;
		this.weight = weight;
		if( roadmap != null )
			this.roadmap = new ArrayList< Stage >( roadmap );
		else
			this.roadmap = new ArrayList< Stage >();
	}

	public Container( JSONObject jsonObject, Warehouse warehouse )
	{
		this( jsonObject.getString( "ID" ),warehouse.updateKnowedOwners(new Owner( jsonObject.getJSONObject( "owner" ))), 0, new ArrayList< Stage >() );

		JSONArray ja = jsonObject.getJSONArray( "roadmap" );
		int lenght = ja.length();
		for( int i = 0; i < lenght; i++ )
		{
			JSONObject jo = ja.getJSONObject( i );
			Stage s = new Stage( jo );
			addStage( s );
		}
	}

	protected void addWeight( float weight )
	{
		if( weight < 0 )
			throw new IllegalArgumentException( "Le poids est inférieur à 0" );

		this.weight += weight;
	}

	protected void removeWeight( float weight )
	{
		if( weight > this.weight )
			throw new IllegalArgumentException( "Le poind que vous essayez d'enlever est supérieur au poids contenu" );

		this.weight -= weight;
	}

	public void addStage( Stage stage )
	{
		roadmap.add( stage );
	}

	/**
	 * Permet d'obtenir la valeur de iD.
	 *
	 * @return the iD
	 */
	public String getID()
	{
		return ID;
	}

	/**
	 * Permet d'obtenir la valeur de owner.
	 *
	 * @return the owner
	 */
	public Owner getOwner()
	{
		return owner;
	}

	/**
	 * Permet d'obtenir la valeur de weight.
	 *
	 * @return the weight
	 */
	public double getWeight()
	{
		return weight;
	}

	/**
	 * Permet d'obtenir la valeur de roadmap.
	 *
	 * @return the roadmap
	 */
	public ArrayList< Stage > getRoadmap()
	{
		return roadmap;
	}

	/**
	 * Permet de définir la valeur de owner
	 *
	 * @param owner
	 *        Le owner.
	 */
	public void setOwner( Owner owner )
	{
		this.owner = owner;
	}

	/**
	 * Permet de définir la valeur de weight
	 *
	 * @param weight
	 *        Le weight.
	 */
	public void setWeight( double weight )
	{
		this.weight = weight;
	}

	public JSONObject toJSON()
	{
		return new JSONObject( this );
	}
	
	public void update(JSONObject jsonObject, Warehouse wh) {
		if(jsonObject.has("owner"))
			owner = wh.updateKnowedOwners(new Owner(jsonObject.getJSONObject( "owner" )));
		if(jsonObject.has("Owner"))
			owner = wh.updateKnowedOwners(new Owner(jsonObject.getJSONObject( "Owner" )));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		return ID.hashCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals( Object obj )
	{
		return this.ID == (( Container ) (obj)).ID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "Container [ID=" + ID + ", owner=" + owner + ", weight=" + weight + ", roadmap=" + roadmap + "]";
	}
}
