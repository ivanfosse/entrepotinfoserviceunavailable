/**
 * 
 */
package com.fosseRenault.Warehouse;

import org.json.JSONObject;

/**
 *
 *
 * @author Jeremie Renault
 * @version 0.1 2017-12-01
 *
 */
public class Owner
{
	private final long GROUP_ID; // Format : entreprise 0 + SIRET; particulier 1 + ID
	private String	   name;
	private String	   adress;

	/**
	 * Constructeur permettant d'initialiser une nouvelle instance de Company.java.
	 *
	 */
	public Owner( long GROUP_ID,
				  String name,
				  String adress )
	{
		// TODO 16 déc. 2017 Check GROUP_ID value
		this.GROUP_ID = GROUP_ID;
		this.name = name;
		this.adress = adress;

	}

	public Owner( JSONObject jsonObject )
	{
		this( jsonObject.getLong( "GROUP_ID" ), jsonObject.getString( "name" ), jsonObject.getString( "adress" ) );
	}

	/**
	 * Permet d'obtenir la valeur de gROUP_ID.
	 *
	 * @return the gROUP_ID
	 */
	public long getGROUP_ID()
	{
		return GROUP_ID;
	}

	/**
	 * Permet d'obtenir la valeur de name.
	 *
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * Permet d'obtenir la valeur de adress.
	 *
	 * @return the adress
	 */
	public String getAdress()
	{
		return adress;
	}

	/**
	 * Permet de définir la valeur de name
	 *
	 * @param name
	 *        Le name.
	 */
	public void setName( String name )
	{
		this.name = name;
	}

	/**
	 * Permet de définir la valeur de adress
	 *
	 * @param adress
	 *        Le adress.
	 */
	public void setAdress( String adress )
	{
		this.adress = adress;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		return new Long( GROUP_ID ).hashCode();
	}

	public JSONObject toJSON()
	{
		return new JSONObject( this );
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "Company [name=" + name + ", adress=" + adress + "]";
	}

}
