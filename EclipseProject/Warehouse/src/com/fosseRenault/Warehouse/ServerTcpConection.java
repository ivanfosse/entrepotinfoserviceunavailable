package com.fosseRenault.Warehouse;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.charset.Charset;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;

import org.json.JSONException;
import org.json.JSONObject;

import com.fosseRenault.Warehouse.Container.Container;
import com.fosseRenault.Warehouse.Container.FluidContainer;
import com.fosseRenault.Warehouse.Container.LooseContainer;
import com.fosseRenault.Warehouse.Container.RefrigeratedContainer;
import com.fosseRenault.Warehouse.Container.SimpleContainer;

import util.exception.BadCertificateException;
import util.exception.CertificateExpiredException;

public class ServerTcpConection extends Thread {
	private BufferedReader input;
    private PrintWriter output;
    private final Socket socketClient;	
    private WarehouseControler warehouseControler;
	private Charset charset = null;
	
	{
		if (Charset.isSupported("UTF-8"))
			charset = Charset.forName("UTF-8");
		else {
			charset = Charset.defaultCharset();
			System.out.println("Unsuported UTF-8 charged default charset :" + charset.name());
		}
	}
	    
    public ServerTcpConection(Socket socketClient, WarehouseControler warehouseControler) {
        this.socketClient = socketClient;
        this.warehouseControler = warehouseControler;
 
        // Association d'un flux d'entr�e et de sortie
        try {
            input = new BufferedReader(new InputStreamReader(socketClient.getInputStream()));
            output = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socketClient.getOutputStream())), true);
        } catch(IOException e) {
            System.err.println("Association des flux impossible : " + e);
            System.exit(-1);
        }
    }
	    
    @Override
    public void run() {
    	Warehouse w = warehouseControler.getWarehouse();
		synchronized (w) {
	    	String receive;
	    	String send;
	    	try {
	    		System.out.println("2 Receive certif");
				receive = input.readLine();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return;
			}
	    	receive = new String(Base64.getDecoder().decode(receive),charset);
	    	System.out.println(receive);
	    	try {
				warehouseControler.getVerifyCertificate().verifyCertificate(receive, "Warehouse");
			} catch (IllegalBlockSizeException | BadPaddingException | JSONException | BadCertificateException
					| CertificateExpiredException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return;
			}
			System.out.println("2 Sending certif");
	    	send = warehouseControler.getWareHouseCertificate();
	    	send = Base64.getEncoder().encodeToString(send.getBytes(charset));
	    	output.println(send);
	    	send = "";
    		try {
    			System.out.println("2 Receiving certif");
    			receive = input.readLine();
    		} catch (IOException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
        	receive = new String(Base64.getDecoder().decode(receive),charset);
        	System.out.println("2 Received new cont "+ receive);
        	JSONObject joC = new JSONObject(receive);
        	Container c = null;
        	System.out.println("2 Determining Type "+ receive);
			switch (joC.getString("ID").substring(0, 2)) {
				case "SC":
					c = new SimpleContainer(joC, w);
					break;
				case "RC":
					c = new RefrigeratedContainer(joC, w);
					break;
				case "FC":
					c = new FluidContainer(joC, w);
					break;
				case "LC":
					c = new LooseContainer(joC, w);
					break;
			}
			if(c == null) {
				JSONObject j = new JSONObject();
				j.put("Result", "Error : unknown");
				send = j.toString();
			}else {
	        	try {
					warehouseControler.getWarehouse().addContainer(c);
					JSONObject j = new JSONObject();
					j.put("Result", "Completed");
					send = j.toString();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					JSONObject j = new JSONObject();
					j.put("Result", "Error : Full");
					send = j.toString();
				}
			}
			System.out.println("2 Sending result");
			System.out.println("sending " + send);
	    	send = Base64.getEncoder().encodeToString(send.getBytes(charset));
	    	output.println(send);
	    	System.out.println("Connection Closed");
	    	try {
				socketClient.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
    }
    
    /**
	 * R�cup�re le charset utilis� pour encrypter les donn�es.
	 */
	public Charset getCharset() {
		return charset;
	}

	/**
	 * D�finie le charset utilis� pour encrypter les donn�es.
	 * 
	 * @param charset
	 *            Le charset
	 */
	public void setCharset(Charset charset) {
		this.charset = charset;
	}
}
