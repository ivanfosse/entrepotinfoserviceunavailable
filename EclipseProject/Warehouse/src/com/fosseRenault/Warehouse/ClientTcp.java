package com.fosseRenault.Warehouse;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.charset.Charset;
import java.util.Base64;

public class ClientTcp {
	Socket socket;
    BufferedReader input;
    PrintWriter output;
	private Charset charset = null;
	
	{
		if (Charset.isSupported("UTF-8"))
			charset = Charset.forName("UTF-8");
		else {
			charset = Charset.defaultCharset();
			System.out.println("Unsuported UTF-8 charged default charset :" + charset.name());
		}
	}
    
    public ClientTcp(InetAddress inetAddress, int port) throws IOException {
    	socket = new Socket(inetAddress, port);
    	input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        output = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
    }

	public Socket getSocket() {
		return socket;
	}

	public String receive() throws IOException {
		return new String(Base64.getDecoder().decode(input.readLine()),charset);
	}
	
	public void send(String str) {
		output.println(Base64.getEncoder().encodeToString(str.getBytes(charset)));
	}
	
	public BufferedReader getInput() {
		return input;
	}

	public PrintWriter getOutput() {
		return output;
	}

	/**
	 * R�cup�re le charset utilis� pour encrypter les donn�es.
	 */
	public Charset getCharset() {
		return charset;
	}

	/**
	 * D�finie le charset utilis� pour encrypter les donn�es.
	 * 
	 * @param charset
	 *            Le charset
	 */
	public void setCharset(Charset charset) {
		this.charset = charset;
	}
}
