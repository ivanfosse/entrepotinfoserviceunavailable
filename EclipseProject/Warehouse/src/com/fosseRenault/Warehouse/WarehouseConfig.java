package com.fosseRenault.Warehouse;
import util.Config;

public class WarehouseConfig {
	private int RSAKeySize;
	private String WarehousesLocation;
	private int SavingTimer;
	
	public WarehouseConfig() {
		Config config = new Config("./configs/WarehouseConfig.json");
		RSAKeySize = Integer.parseInt(config.get("RSAKeySize", "2048"));
		WarehousesLocation = config.get("WarehousesLocation","./warehouses");
		SavingTimer = Integer.parseInt(config.get("SavingTimer","5"));
	}

	public int getRSAKeySize() {
		return RSAKeySize;
	}

	public String getWarehousesLocation() {
		return WarehousesLocation;
	}

	public int getSavingTimer() {
		return SavingTimer;
	}
	
	
	
}
