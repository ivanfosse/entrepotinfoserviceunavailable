/**
 * 
 */
package com.fosseRenault.Warehouse;

import java.util.concurrent.ConcurrentHashMap;

import org.json.JSONObject;

import com.fosseRenault.Warehouse.Container.Container;
import com.fosseRenault.Warehouse.Container.FluidContainer;
import com.fosseRenault.Warehouse.Container.LooseContainer;
import com.fosseRenault.Warehouse.Container.RefrigeratedContainer;
import com.fosseRenault.Warehouse.Container.SimpleContainer;
import com.fosseRenault.Warehouse.exceptions.DuplicateContainerException;
import com.fosseRenault.Warehouse.exceptions.DuplicateSpotException;
import com.fosseRenault.Warehouse.exceptions.WarehouseFullException;

/**
 *
 *
 * @author Jeremie Renault
 * @version 0.1 2017-12-01
 *
 */
public class Warehouse {
	private Owner owner;
	private final String NAME;
	private String adress;
	private int nbRows;
	private int nbColumns;
	private int height;
	private ConcurrentHashMap<Long, Owner> knowedOwners; 
	private ConcurrentHashMap<String, Container> storedContainers;
	private ConcurrentHashMap<Spot, Container> containers;
	private ConcurrentHashMap<Container, Spot> containersinv;
	private int[][] nbPiledContainers;

	public int[][] getNbPiledContainers() {
		return nbPiledContainers;
	}

	/**
	 * Constructeur permettant d'initialiser une nouvelle instance de
	 * Warehouse.java.
	 *
	 */
	public Warehouse(Owner owner, String name, String adress, int nbRows, int nbColumns, int height) {
		this.knowedOwners = new ConcurrentHashMap<Long, Owner>();
		this.storedContainers = new ConcurrentHashMap<String, Container>();
		this.containers = new ConcurrentHashMap<Spot, Container>();
		this.containersinv = new ConcurrentHashMap<>();
		this.nbPiledContainers = new int[nbRows][nbColumns];

		this.owner = updateKnowedOwners(owner);
		this.NAME = name;
		this.adress = adress;
		this.nbRows = nbRows;
		this.nbColumns = nbColumns;
		this.height = height;
	}

	public Warehouse(JSONObject jsonObject) {
		this(null, jsonObject.getString("NAME"), jsonObject.getString("adress"), jsonObject.getInt("nbRows"),
				jsonObject.getInt("nbColumns"), jsonObject.getInt("height"));

		this.owner = new Owner(jsonObject.getJSONObject("owner"));
		this.owner = this.updateKnowedOwners(this.owner);

		// TODO 19 déc. 2017 ajouter les conteneurs
		JSONObject jo = jsonObject.getJSONObject("containers");

		for (String key : jo.keySet()) {
			JSONObject joC = jo.getJSONObject(key);
			switch (joC.getString("ID").substring(0, 2)) {
			case "SC":
				loadContainer(new SimpleContainer(joC, this), new Spot(key));
				break;
			case "RC":
				loadContainer(new RefrigeratedContainer(joC, this), new Spot(key));
				break;
			case "FC":
				loadContainer(new FluidContainer(joC, this), new Spot(key));
				break;
			case "LC":
				loadContainer(new LooseContainer(joC, this), new Spot(key));
				break;
			}
		}

	}

	private void loadContainer(Container container, Spot spot) {

		nbPiledContainers[spot.getRow()][spot.getColumn()]++;
		containers.put(spot, container);
		containersinv.put(container, spot);
		storedContainers.put(container.getID(), container);
	}

	private void addContainer(Container container, Spot spot) throws DuplicateSpotException {
		if (containers.containsKey(spot))
			throw new DuplicateSpotException("L'emplacement " + spot + " est déjà pris");

		nbPiledContainers[spot.getRow()][spot.getColumn()]++;
		containers.put(spot, container);
		containersinv.put(container, spot);
		container.addStage(new Stage(owner.getName(), NAME, adress, spot));
		storedContainers.put(container.getID(), container);
	}

	private void addContainer(Container container, int row, int column, int height) throws DuplicateSpotException {
		addContainer(container, new Spot(row, column, height));
	}

	private void addContainer(Container container, int row, int column) throws WarehouseFullException {
		if (nbPiledContainers[row][column] >= height)
			throw new WarehouseFullException();

		try {
			addContainer(container, row, column, nbPiledContainers[row][column]);
		} catch (DuplicateSpotException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 *
	 * @param container
	 * @throws WarehouseFullException
	 * @throws DuplicateContainerException
	 */
	public void addContainer(Container container) throws WarehouseFullException, DuplicateContainerException {
		if (storedContainers.containsKey(container.getID()))
			throw new DuplicateContainerException("Le conteneur " + container.getID() + " existe déjà");

		boolean found = false;
		int row = -1;
		int col = -1;
		while (!found && row < nbRows - 1) {
			row++;
			col = -1;
			while (!found && col < nbColumns - 1) {
				col++;
				if (nbPiledContainers[row][col] < height)
					found = true;
			}
		}

		if (!found)
			throw new WarehouseFullException();

		addContainer(container, row, col);
	}

	public Container removeContainer(int row, int column, int height) {
		return removeContainer(new Spot(row, column, height));
	}
	
	public Container removeContainer(Spot spot) {
		Container container = containers.remove(spot);
		if(container == null)
			return null;
		containersinv.remove(container);
		nbPiledContainers[spot.getRow()][spot.getColumn()]--;
		storedContainers.remove(container.getID());

		if (container != null) {
			Container c;
			int h = spot.getHeight();
			while ((c = containers.remove(new Spot(spot.getRow(), spot.getColumn(), h + 1))) != null) {

				containersinv.remove(c);
				Spot sp = new Spot(spot.getRow(), spot.getColumn(), h);
				containers.put(sp, c);
				containersinv.put(c, sp);
				c.addStage(new Stage(owner.getName(), NAME, adress, sp));
				h++;
			}
		}

		return container;
	}
	
	public Container removeContainer(Container c) {
		return removeContainer(containersinv.get(c));
	}
	
	public Container removeContainer(String Id) {
		Container c = getContainer(Id);
		if(c == null)
			return null;
		return removeContainer(containersinv.get(c));
	}

	public boolean isStored(Container container) {
		return storedContainers.containsKey(container.getID());
	}

	public Container getContainer(Spot spot) {
		Container c;

		c = containers.get(spot);

		return c;
	}

	public Container getContainer(int row, int column, int height) {
		return getContainer(new Spot(row, column, height));
	}

	public Owner updateKnowedOwners(Owner owner) {
		if (owner == null)
			return null;

		Owner o = knowedOwners.get(owner.getGROUP_ID());

		if (o == null) {
			o = owner;
			knowedOwners.put(o.getGROUP_ID(), o);
		} else {
			o.setAdress(owner.getAdress());
			o.setName(owner.getName());
		}

		return o;
	}
	
	public Container getContainer(String id) {
		return storedContainers.get(id);
	}

	public Spot getSpot(Container container) {
		// TODO 18 déc. 2017 Ajout HashMap< Container, Spot > cascade dans les
		// modification
		return containersinv.get(container);
	}

	public Spot getSpot(String id) {
		// TODO 18 déc. 2017 comme le précédent
		Container c = getContainer(id);
		if(c==null)
			return null;
		return getSpot(c);
		
	}

	/**
	 * Permet d'obtenir la valeur de owner.
	 *
	 * @return the owner
	 */
	public Owner getOwner() {
		return owner;
	}

	/**
	 * Permet d'obtenir la valeur de name.
	 *
	 * @return the name
	 */
	public String getNAME() {
		return NAME;
	}

	/**
	 * Permet d'obtenir la valeur de adress.
	 *
	 * @return the adress
	 */
	public String getAdress() {
		return adress;
	}

	/**
	 * Permet d'obtenir la valeur de nbRows.
	 *
	 * @return the nbRows
	 */
	public int getNbRows() {
		return nbRows;
	}

	/**
	 * Permet d'obtenir la valeur de nbColumns.
	 *
	 * @return the nbColumns
	 */
	public int getNbColumns() {
		return nbColumns;
	}

	/**
	 * Permet d'obtenir la valeur de height.
	 *
	 * @return the height
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * Permet d'obtenir la valeur de containers.
	 *
	 * @return the containers
	 */
	public ConcurrentHashMap<Spot, Container> getContainers() {
		return containers;
	}

	/**
	 * Permet de définir la valeur de owner
	 *
	 * @param owner
	 *            Le owner.
	 */
	public void setOwner(Owner owner) {
		this.owner = owner;
	}

	/**
	 * Permet de définir la valeur de adress
	 *
	 * @param adress
	 *            Le adress.
	 */
	public void setAdress(String adress) {
		this.adress = adress;
	}

	/**
	 * Permet de définir la valeur de nbRows
	 *
	 * @param nbRows
	 *            Le nbRows.
	 */
	public void setNbRows(int nbRows) {
		this.nbRows = nbRows;
	}

	/**
	 * Permet de définir la valeur de nbColumns
	 *
	 * @param nbColumns
	 *            Le nbColumns.
	 */
	public void setNbColumns(int nbColumns) {
		this.nbColumns = nbColumns;
	}

	/**
	 * Permet de définir la valeur de height
	 *
	 * @param height
	 *            Le height.
	 */
	public void setHeight(int height) {
		this.height = height;
	}

	/**
	 * Permet de définir la valeur de containers
	 *
	 * @param containers
	 *            Le containers.
	 */
	public void setContainers(ConcurrentHashMap<Spot, Container> containers) {
		this.containers = containers;
	}

	public JSONObject toJSON() {
		return new JSONObject(this);
	}
}
