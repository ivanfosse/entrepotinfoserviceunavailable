/**
 * 
 */
package com.fosseRenault.Warehouse;

import org.json.JSONObject;

/**
 *
 *
 * @author Jeremie Renault
 * @version 0.1 2017-12-01
 *
 */
public class Stage
{
	private String containerCompanyName;
	private String warehouseName;
	private String warehouseAdress;
	private Spot   spot;

	/**
	 * Constructeur permettant d'initialiser une nouvelle étape sur la feuille de route.
	 *
	 */
	public Stage( String containerCompanyName,
				  String warehouseName,
				  String warehouseAdress,
				  Spot spot )
	{
		this.containerCompanyName = containerCompanyName;
		this.warehouseName = warehouseName;
		this.warehouseAdress = warehouseAdress;
		this.spot = new Spot( spot );
	}
	
	public Stage( JSONObject jsonObject )
	{
		this( jsonObject.getString( "containerCompanyName"), jsonObject.getString( "warehouseName"), jsonObject.getString( "warehouseAdress"), new Spot( jsonObject.getJSONObject( "spot" )));
	}

	/**
	 * Permet d'obtenir la valeur de containerCompanyName.
	 *
	 * @return the containerCompanyName
	 */
	public String getContainerCompanyName()
	{
		return containerCompanyName;
	}

	/**
	 * Permet d'obtenir la valeur de warehouseName.
	 *
	 * @return the warehouseName
	 */
	public String getWarehouseName()
	{
		return warehouseName;
	}

	/**
	 * Permet d'obtenir la valeur de warehouseAdress.
	 *
	 * @return the warehouseAdress
	 */
	public String getWarehouseAdress()
	{
		return warehouseAdress;
	}

	/**
	 * Permet d'obtenir la valeur de spot.
	 *
	 * @return the spot
	 */
	public Spot getSpot()
	{
		return spot;
	}
	
	public JSONObject toJSON()
	{
		return new JSONObject( this );
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "Stage [containerCompanyName=" + containerCompanyName + ", warehouseName=" + warehouseName + ", warehouseAdress=" + warehouseAdress
				+ ", spot=" + spot + "]";
	}
	
	public static void main( String[] args )
	{
		Stage stage = new Stage( "La fosse des renaults", "Vault 11", "Désert de Mojave", new Spot( 1, 1, 1 ) );
		System.out.println( stage );
	}
}
