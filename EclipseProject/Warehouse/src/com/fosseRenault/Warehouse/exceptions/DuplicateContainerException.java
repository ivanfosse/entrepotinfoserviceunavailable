/**
 * 
 */
package com.fosseRenault.Warehouse.exceptions;

/**
 *
 *
 * @author Jeremie Renault
 * @version 0.1 2017-12-15
 *
 */
public class DuplicateContainerException extends Exception
{

	/**
	 * Constructeur permettant d'initialiser une nouvelle instance de duplicateContainerException.java.
	 *
	 */
	public DuplicateContainerException()
	{
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur permettant d'initialiser une nouvelle instance de duplicateContainerException.java.
	 *
	 * @param message
	 */
	public DuplicateContainerException( String message )
	{
		super( message );
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur permettant d'initialiser une nouvelle instance de duplicateContainerException.java.
	 *
	 * @param cause
	 */
	public DuplicateContainerException( Throwable cause )
	{
		super( cause );
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur permettant d'initialiser une nouvelle instance de duplicateContainerException.java.
	 *
	 * @param message
	 * @param cause
	 */
	public DuplicateContainerException( String message,
										Throwable cause )
	{
		super( message, cause );
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur permettant d'initialiser une nouvelle instance de duplicateContainerException.java.
	 *
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public DuplicateContainerException( String message,
										Throwable cause,
										boolean enableSuppression,
										boolean writableStackTrace )
	{
		super( message, cause, enableSuppression, writableStackTrace );
		// TODO Auto-generated constructor stub
	}

}
