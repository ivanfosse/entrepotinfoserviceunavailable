/**
 * 
 */
package com.fosseRenault.Warehouse.exceptions;

/**
 *
 *
 * @author Jeremie Renault
 * @version 0.1 2017-12-11
 *
 */
public class WarehouseFullException extends Exception
{

	/**
	 * Constructeur permettant d'initialiser une nouvelle instance de WarehouseFullException.java.
	 *
	 */
	public WarehouseFullException()
	{
		super();
	}

	/**
	 * Constructeur permettant d'initialiser une nouvelle instance de WarehouseFullException.java.
	 *
	 * @param message
	 */
	public WarehouseFullException( String message )
	{
		super( message );
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur permettant d'initialiser une nouvelle instance de WarehouseFullException.java.
	 *
	 * @param cause
	 */
	public WarehouseFullException( Throwable cause )
	{
		super( cause );
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur permettant d'initialiser une nouvelle instance de WarehouseFullException.java.
	 *
	 * @param message
	 * @param cause
	 */
	public WarehouseFullException( String message,
								   Throwable cause )
	{
		super( message, cause );
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur permettant d'initialiser une nouvelle instance de WarehouseFullException.java.
	 *
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public WarehouseFullException( String message,
								   Throwable cause,
								   boolean enableSuppression,
								   boolean writableStackTrace )
	{
		super( message, cause, enableSuppression, writableStackTrace );
		// TODO Auto-generated constructor stub
	}

}
