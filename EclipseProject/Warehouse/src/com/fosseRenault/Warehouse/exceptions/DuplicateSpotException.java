/**
 * 
 */
package com.fosseRenault.Warehouse.exceptions;

/**
 *
 *
 * @author Jeremie Renault
 * @version 0.1 2017-12-19
 *
 */
public class DuplicateSpotException extends Exception
{

	/**
	 * Constructeur permettant d'initialiser une nouvelle instance de DuplicateSpotException.java.
	 *
	 */
	public DuplicateSpotException()
	{
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur permettant d'initialiser une nouvelle instance de DuplicateSpotException.java.
	 *
	 * @param arg0
	 */
	public DuplicateSpotException( String arg0 )
	{
		super( arg0 );
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur permettant d'initialiser une nouvelle instance de DuplicateSpotException.java.
	 *
	 * @param arg0
	 */
	public DuplicateSpotException( Throwable arg0 )
	{
		super( arg0 );
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur permettant d'initialiser une nouvelle instance de DuplicateSpotException.java.
	 *
	 * @param arg0
	 * @param arg1
	 */
	public DuplicateSpotException( String arg0,
								   Throwable arg1 )
	{
		super( arg0, arg1 );
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur permettant d'initialiser une nouvelle instance de DuplicateSpotException.java.
	 *
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @param arg3
	 */
	public DuplicateSpotException( String arg0,
								   Throwable arg1,
								   boolean arg2,
								   boolean arg3 )
	{
		super( arg0, arg1, arg2, arg3 );
		// TODO Auto-generated constructor stub
	}

}
