package com.fosseRenault.Warehouse;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerTcp {
	private int portEcoute;
	private ServerSocket socketServeur = null;
	private boolean stop = false;
	private WarehouseControler warehouseControler;
	
	public ServerTcp(int portEcoute, WarehouseControler warehouseControler) {
		this.warehouseControler = warehouseControler;
		this.portEcoute = portEcoute;
	}
	
	public void launch() throws IOException {
		stop = false;
        socketServeur = new ServerSocket(portEcoute);
		while (!stop) {
			try {
	            Socket socketClient;
	            while(true) {
	            socketClient = socketServeur.accept();
	            ServerTcpConection t = new ServerTcpConection(socketClient, warehouseControler);
	            t.start();
	            }
	        } catch(IOException e) {
	            System.err.println("Erreur lors de l'attente d'une connexion : " + e);
	            System.exit(-1);
	        }
		}
		socketServeur.close();
		socketServeur = null;
	}

	 
}
