package com.fosseRenault.Warehouse;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Scanner;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.json.JSONException;
import org.json.JSONObject;

import com.fosseRenault.Warehouse.Container.Container;
import com.fosseRenault.Warehouse.Container.FluidContainer;
import com.fosseRenault.Warehouse.Container.LooseContainer;
import com.fosseRenault.Warehouse.Container.RefrigeratedContainer;
import com.fosseRenault.Warehouse.Container.SimpleContainer;
import com.fosseRenault.Warehouse.exceptions.DuplicateContainerException;
import com.fosseRenault.Warehouse.exceptions.WarehouseFullException;

import util.Console;
import util.Logger;
import util.VerifyCertificate;
import util.exception.BadCertificateException;
import util.exception.CertificateExpiredException;
import util.exception.WrongCertificateException;

public class WarehouseControler {
	private WarehouseConfig warehouseConfig;
	private Warehouse warehouse;
	private String name;
	private int port;
	private Thread savingThread;
	private Console console;
	private ServerUdp serverUdp;
	private ServerTcp serverTcp;
	private Thread udpThread;
	private Thread tcpThread;
	private boolean working;
	private VerifyCertificate verifyCertificate;
	private String wareHouseCertificate;

	public WarehouseControler(String name, int port) throws JSONException, IOException {
		System.out.println("Launching Existing Warehouse " + name+ " " + port);
		this.name = name;
		warehouseConfig = new WarehouseConfig();
		if(new File(warehouseConfig.getWarehousesLocation() + "/" + name + "warehousecontent.json").exists()) {
			warehouse = new Warehouse(new JSONObject(new String(Files.readAllBytes(Paths.get(warehouseConfig.getWarehousesLocation() + "/" + name + "warehousecontent.json")))));
			
		}
		loadCertif();
		savingThread = new Thread(new Runnable() {

			@Override
			public void run() {
				while (true) {
					try {
						Thread.sleep(warehouseConfig.getSavingTimer()*1000);
					} catch (InterruptedException e) {
					}
					try {
						if(warehouse != null)
							synchronized (warehouse) {
								System.out.println("Saving " + LocalDateTime.now());
								save();
							}
					} catch (JSONException | IOException e1) {

					}
				}
			}
		});
		savingThread.start();

		serverTcp = new ServerTcp(port, this);
		serverUdp = new ServerUdp(port, 64000) {

			@Override
			public void receivePacket(String string, InetAddress adress, int port) {
				new Thread(new Runnable() {
					@Override
					public void run() {
						WarehouseControler.this.receivePacket(string, adress, port);
					}
				}).start();
			}
		};
		udpThread = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					serverUdp.launch();
				} catch (SocketException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.exit(1);
				}
			}
		});
		tcpThread = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					serverTcp.launch();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.exit(1);
				}
			}
		});
		tcpThread.start();
		udpThread.start();

		initConsole();
	}

	public void receivePacket(String string, InetAddress adress, int port) {
		string = new String(string);
		try {
			adress = InetAddress.getByName(adress.getHostAddress());
		} catch (UnknownHostException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		System.out.println("Received " + string.length() + " adress " + adress + " port " + port);
		JSONObject json; 
		try {
			json = new JSONObject(string);
		}catch(JSONException e) {
			try {
				serverUdp.sendPacket("{\"Error\":\"Need Json to command\"}", adress, port);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return;
		}
		if(working) {
			try {
				System.out.println("Exec receiveAsk");
				String answ = receiveAsk(json);
				System.out.println("Answer receiveAsk " + answ);
				serverUdp.sendPacket(answ, adress, port);
				System.out.println("Sent");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else {
			if(!json.has("Type")){
				try {
					serverUdp.sendPacket("{\"Error\":\"No Type Found\"}", adress, port);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				return;
			}
			String type = json.getString("Type");
			if (type.equals("Initialise")) {
				try {
					verifyCertificate = new VerifyCertificate(json.getString("GeneralCertificate"));
					verifyCertificate.verifyCertificate(json.getString("ServerCertificate"),"Server");
			        wareHouseCertificate = json.getString("WarehouseCertificate");
			        try {
						saveCertif();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			        verifyCertificate.verifyCertificate(wareHouseCertificate, "Warehouse");
			        if(warehouse == null) {
			        	String adresse = json.getString("Adresse");
			        	Owner ow = new Owner(json.getJSONObject("Owner"));
			        	warehouse = new Warehouse(ow, name, adresse, 4, 5, 6);
			        }else {
			        	warehouse.setAdress(json.getString("Adresse"));
			        	warehouse.setOwner(new Owner(json.getJSONObject("Owner")));
			        }
			        try {
			        	working = true;
						serverUdp.sendPacket("{\"Result\":\"Ok\"}", adress, port);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					return;
				} catch (InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException
						| NoSuchPaddingException | JSONException | IllegalBlockSizeException | BadPaddingException
						| WrongCertificateException | CertificateExpiredException | BadCertificateException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					try {
						serverUdp.sendPacket("{\"Error\":\"Error Initialise\"}", adress, port);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					return;
				}
			}else {
				try {
					serverUdp.sendPacket("{\"Error\":\"Not Initialised\"}", adress, port);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
		System.out.println("Finished exec Command");
	}

	private String receiveAsk(JSONObject json) {
		try {
			System.out.println("TestType");
			if(!json.has("Type"))
				return "{\"Error\":\"No Type Found\"}";
			String type = json.getString("Type");
			if (type.equals("WarehouseInfo")) {
				JSONObject answer = new JSONObject();
				answer.put("Height", warehouse.getHeight());
				answer.put("Row", warehouse.getNbRows());
				answer.put("Column", warehouse.getNbColumns());
				answer.put("Values", warehouse.getNbPiledContainers());
				answer.put("Adress", warehouse.getAdress());
				answer.put("Name", warehouse.getNAME());
				return answer.toString();
			} else if (type.equals("AddContainer")) {
				String str = json.getString("ContainerType");
				if (str.equals("FluidContainer")) {
					int ID = json.getInt("ID");
					Owner owner = new Owner(json.getJSONObject("Owner"));
					owner = warehouse.updateKnowedOwners(owner);
					String fluidType = json.getString("FluidType");
					float density = (float) json.getDouble("Density");
					double volume = json.getDouble("Volume");
					FluidContainer container = new FluidContainer(ID, owner, fluidType, density, volume);
					try {
						warehouse.addContainer(container);
						JSONObject answer = new JSONObject();
						answer.put("ID", container.getID());
						answer.put("Answer", "Completed");
						return answer.toString();
					} catch (WarehouseFullException | DuplicateContainerException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						JSONObject answer = new JSONObject();
						answer.put("Answer", "Error : Duplication of ID");
						return answer.toString();
					}
				} else if (str.equals("LooseContainer")) {
					int ID = json.getInt("ID");
					Owner owner = new Owner(json.getJSONObject("Owner"));
					owner = warehouse.updateKnowedOwners(owner);
					String looseType = json.getString("LooseType");
					LooseContainer container = new LooseContainer(ID, owner, looseType);
					try {
						warehouse.addContainer(container);
						JSONObject answer = new JSONObject();
						answer.put("ID", container.getID());
						answer.put("Answer", "Completed");
						return answer.toString();
					} catch (WarehouseFullException | DuplicateContainerException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						JSONObject answer = new JSONObject();
						answer.put("Answer", "Error : Duplication of ID");
						return answer.toString();
					}
				} else if (str.equals("RefrigeratedContainer")) {
					int ID = json.getInt("ID");
					Owner owner = new Owner(json.getJSONObject("Owner"));
					owner = warehouse.updateKnowedOwners(owner);
					float temperature = json.getInt("Temperature");
					long workingDuration = json.getLong("WorkingDuration");
					JSONObject merchandises = json.getJSONObject("Merchandises");
					HashMap<String, Float> hm = new HashMap<>();
					RefrigeratedContainer container = new RefrigeratedContainer(ID, owner, temperature, workingDuration,
							LocalDateTime.now(), hm);
					try {
						warehouse.addContainer(container);
						JSONObject answer = new JSONObject();
						answer.put("ID", container.getID());
						answer.put("Answer", "Completed");
						return answer.toString();
					} catch (WarehouseFullException | DuplicateContainerException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						JSONObject answer = new JSONObject();
						answer.put("Answer", "Error : Duplication of ID");
						return answer.toString();
					}
				} else if (str.equals("SimpleContainer")) {
					int ID = json.getInt("ID");
					Owner owner = new Owner(json.getJSONObject("Owner"));
					owner = warehouse.updateKnowedOwners(owner);
					JSONObject merchandises = json.getJSONObject("Merchandises");
					HashMap<String, Float> hm = new HashMap<>();

					SimpleContainer container = new SimpleContainer(ID, owner, hm);
					try {
						warehouse.addContainer(container);
						JSONObject answer = new JSONObject();
						answer.put("ID", container.getID());
						answer.put("Answer", "Completed");
						return answer.toString();
					} catch (WarehouseFullException | DuplicateContainerException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						JSONObject answer = new JSONObject();
						answer.put("Answer", "Error : Duplication of ID");
						return answer.toString();
					}
				}
			} else if (type.equals("ModifyContainer")) {
				String ID = json.getString("ID");
				Container cont = warehouse.getContainer(ID);
				if (cont == null) {
					JSONObject answer = new JSONObject();
					answer.put("Answer", "Error : Unknown Container");
					return answer.toString();
				} else {
					try {
						cont.update(json, warehouse);
					} catch (Exception e) {
						e.printStackTrace();
						JSONObject answer = new JSONObject();
						answer.put("Answer", "Error : Updating failed");
						return answer.toString();
					}
					JSONObject answer = new JSONObject();
					answer.put("Answer", "Completed");
					return answer.toString();
				}
			} else if (type.equals("GetContainer")) {
				String ID = json.getString("ID");
				Container cont = warehouse.getContainer(ID);
				if (cont == null) {
					JSONObject answer = new JSONObject();
					answer.put("Answer", "Error : Unknown Container");
					return answer.toString();
				} else {
					JSONObject answer = new JSONObject();
					answer = cont.toJSON();
					return answer.toString();
				}
			} else if (type.equals("RemoveContainer")) {
				String ID = json.getString("ID");
				Container cont = warehouse.removeContainer(ID);
				if (cont == null) {
					JSONObject answer = new JSONObject();
					answer.put("Answer", "Error : Unknown Container");
					return answer.toString();
				} else {
					JSONObject answer = new JSONObject();
					answer.put("Answer", "Completed");
					return answer.toString();
				}
			} else if (type.equals("MoveContainer")) {
				String ID = json.getString("ID");
				int port = json.getInt("Port");
				String Ip = json.getString("IpAdress");
				Container cont = warehouse.getContainer(ID);
				if (cont == null) {
					JSONObject answer = new JSONObject();
					answer.put("Answer", "Error : Unknown Container");
					return answer.toString();
				} else {
					JSONObject jsoncont = cont.toJSON();
					try {
						ClientTcp client = new ClientTcp(InetAddress.getByName(Ip), port);
						System.out.println("Sending certif");
						client.send(wareHouseCertificate);
						System.out.println("Receiving certif");
						String received = client.receive();
						try {
							System.out.println("Validating certif");
							verifyCertificate.verifyCertificate(received, "Warehouse");
							System.out.println("Sending Cont");
							client.send(jsoncont.toString());
							System.out.println("Receive Ans");
							String received2 = client.receive();
							System.out.println("Receivedjeej " + received2);
							JSONObject result = new JSONObject(received2);
							if (!result.has("Result")) {
								JSONObject answer = new JSONObject();
								answer = result;
								return answer.toString();
							}
							if (result.getString("Result").equals("Completed")) {
								warehouse.removeContainer(ID);
								JSONObject answer = new JSONObject();
								answer.put("Answer", "Completed");
								return answer.toString();
							} else {
								JSONObject answer = new JSONObject();
								answer.put("Answer", result.getString("Result"));
								return answer.toString();
							}
						} catch (IllegalBlockSizeException | BadPaddingException | JSONException
								| BadCertificateException | CertificateExpiredException e) {
							e.printStackTrace();
							JSONObject answer = new JSONObject();
							answer.put("Answer", "Error : Warehouse Internal Error");
							return answer.toString();
						}
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						JSONObject answer = new JSONObject();
						answer.put("Answer", "Error : Can't connect to Warehouse");
						return answer.toString();
					}
				}
			}
			return "{\"Error\":\"Warehouse Internal Error\"}";
		} catch (Exception e) {
			System.out.println("ERROR");
			e.printStackTrace();
			return "{}";
		}
	}

	private void loadCertif() throws IOException {
		if(new File("./certificates/" + name + "warehousecertificate.json").exists())
		wareHouseCertificate = new String(
				Files.readAllBytes(new File("./certificates/" + name + "warehousecertificate.json").toPath()));
	}

	private void saveCertif() throws IOException {
		try {
			Files.createDirectories(Paths.get("./certificates/"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		BufferedWriter bw = new BufferedWriter(
				new FileWriter(new File("./certificates/" + name + "warehousecertificate.json")));
		bw.write(wareHouseCertificate);
		bw.flush();
		bw.close();
		bw = null;
	}

	private void initConsole() {
		console = new Console();
		console.launch();
		System.exit(0);
	}

	public void save() throws JSONException, IOException {
		JSONObject json;
		json = warehouse.toJSON();
		try {
			Files.createDirectories(Paths.get(warehouseConfig.getWarehousesLocation()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		BufferedWriter bw = new BufferedWriter(new FileWriter(
				new File(warehouseConfig.getWarehousesLocation() + "/" + name + "warehousecontent.json")));
		bw.write(json.toString());
		bw.flush();
		bw.close();
		bw = null;
	}

	public static void main(String[] args) throws JSONException, IOException, InterruptedException {
		/*
		 * JSONObject jsonask = new JSONObject(); jsonask.put("ID","RC0000005656");
		 * jsonask.put("Port",27001); jsonask.put("IpAdress","localhost");
		 * jsonask.put("Type", "MoveContainer");
		 * 
		 * //WarehouseControler warehouseControler = new WarehouseControler("test2",
		 * 27000,"ardress", new Owner(0,"n","a"));
		 * 
		 * /*System.out.println(jsonask.toString(3));
		 * System.out.println(warehouseControler.receiveAsk(jsonask.toString(3)));
		 * System.out.println(jsonask.toString(3)); WarehouseControler
		 * warehouseControler = new WarehouseControler("test2", 27000);
		 * Thread.sleep(500);
		 * System.out.println(warehouseControler.receiveAsk(jsonask.toString(3)));
		 */
		Logger.initialiseStream();
		if (args.length == 2) {
			String name = args[0];
			int port = Integer.parseInt(args[1]);
			WarehouseControler warehouseControler = new WarehouseControler(name, port);
		}else {
			Scanner sc = new Scanner(System.in);
			System.out.print("Name :");
			String s = sc.nextLine();
			s= s.replaceAll("[^A-Za-z0-9]", "");
			int p = sc.nextInt();
			WarehouseControler warehouseControler = new WarehouseControler(s, p);
			
		}
	}

	public VerifyCertificate getVerifyCertificate() {
		return verifyCertificate;
	}

	public String getWareHouseCertificate() {
		return wareHouseCertificate;
	}

	public Warehouse getWarehouse() {
		return warehouse;
	}

}
