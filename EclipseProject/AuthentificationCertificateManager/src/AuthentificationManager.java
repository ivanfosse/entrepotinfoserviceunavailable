import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.json.JSONException;

import com.sun.net.httpserver.HttpServer;

import util.Console;
import util.EncryptionManager;
import util.Logger;
import util.VerifyCertificate;
import util.exception.BadCertificateException;
import util.exception.CertificateExpiredException;
import util.exception.WrongCertificateException;

public class AuthentificationManager {
	private AuthentificationManagerConfig authentificationManagerConfig;
	private EncryptionManager encryptionManager;
	private Console console;
	private HttpServer httpServer;
	
	private ServerCertificateManager serverCertificateManager;
	private GeneralCertificateManager generalCertificateManager;
	private UserCertificateManager userCertificateManager;
	private WarehouseCertificateManager warehouseCertificateManager;

	public AuthentificationManager() {
		authentificationManagerConfig = new AuthentificationManagerConfig();
		encryptionManager = new EncryptionManager(authentificationManagerConfig.getRSAKeySize(), "AuthentificationManagerKeys");
		try {
			generalCertificateManager = new GeneralCertificateManager(encryptionManager);
			serverCertificateManager = new ServerCertificateManager(encryptionManager);
			warehouseCertificateManager = new WarehouseCertificateManager(encryptionManager, new VerifyCertificate(generalCertificateManager.getGeneralCertificate()));
		} catch (NumberFormatException | RemoteException | InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException | JSONException | IllegalBlockSizeException | BadPaddingException | WrongCertificateException | CertificateExpiredException | BadCertificateException e) {
			System.out.println("Impossible de cr�er le g�n�rateur de certificat g�n�raux");
			e.printStackTrace();
			System.exit(1);
		}
		try {
			Naming.rebind(authentificationManagerConfig.getGeneralCertificateManagerRMIObjectUrl(), generalCertificateManager);
		} catch (MalformedURLException | RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
		try {
			Naming.rebind(authentificationManagerConfig.getServerCertificateManagerRMIObjectUrl(), serverCertificateManager);
		} catch (MalformedURLException | RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
		try {
			Naming.rebind(authentificationManagerConfig.getWarehouseCertificateManagerRMIObjectUrl(), warehouseCertificateManager);
		} catch (MalformedURLException | RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
		try {
			httpServer = HttpServer.create(new InetSocketAddress(authentificationManagerConfig.getHttpServerPort()), 0);
        } catch(IOException e) {
            System.err.println("Erreur lors de la cr�ation du serveur " + e);
            System.exit(-1);
        }
		try {
			userCertificateManager = new UserCertificateManager(authentificationManagerConfig, encryptionManager, generalCertificateManager);
		} catch (InvalidKeyException | RemoteException | NoSuchAlgorithmException | InvalidKeySpecException
				| NoSuchPaddingException | JSONException | IllegalBlockSizeException | BadPaddingException
				| WrongCertificateException | CertificateExpiredException | BadCertificateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
		httpServer.createContext("/login.jeej", userCertificateManager);
		httpServer.setExecutor(null);
		httpServer.start();
		
		initConsole();
	}
	
	private void initConsole(){
		console = new Console();
		console.launch();
		System.exit(0);
	}
	
	
	public static void main(String[] args) {
		Logger.initialiseStream();
		AuthentificationManager auth = new AuthentificationManager();
	}
}
