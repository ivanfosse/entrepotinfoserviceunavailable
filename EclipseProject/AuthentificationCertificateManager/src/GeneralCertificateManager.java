import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;

import org.json.JSONException;
import org.json.JSONObject;

import rmiIterface.RmiGeneralCertificateManager;
import util.EncryptionManager;

public class GeneralCertificateManager extends UnicastRemoteObject implements RmiGeneralCertificateManager {
	private static final long serialVersionUID = 3714395172079272834L;
	private EncryptionManager encrypt;
	private String certificate;
	
	public GeneralCertificateManager(EncryptionManager encrypt) throws RemoteException { 
		this.encrypt = encrypt;
		try {
			JSONObject file = new JSONObject();
			JSONObject certif = new JSONObject();
			//Contenue Certificat
			certif.put("Type", "General");
			certif.put("PublicKey", encrypt.getPublicKey());
			//Fin Certificat
			String certifstring = certif.toString(3);
			String sha = encrypt.getSha().hash(certifstring);
			file.put("Certificate", certifstring);
			file.put("Signature", encrypt.getRsa().encryptWithPrivate(sha));
			certificate = file.toString(3);
		} catch (JSONException | IllegalBlockSizeException | BadPaddingException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public String getGeneralCertificate() throws RemoteException{
		return certificate;
	}
	
}
