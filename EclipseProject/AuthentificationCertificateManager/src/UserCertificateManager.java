import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URLDecoder;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.time.LocalDateTime;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.json.JSONException;
import org.json.JSONObject;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import rmiIterface.RmiUsersManager;
import util.EncryptionManager;
import util.Logger;
import util.Logger.Type;
import util.VerifyCertificate;
import util.exception.BadCertificateException;
import util.exception.CertificateExpiredException;
import util.exception.WrongCertificateException;


public class UserCertificateManager implements HttpHandler{
	private AuthentificationManagerConfig authentificationManagerConfig;
	private EncryptionManager encryptionManager;
	private String generalCertificate;
	private RmiUsersManager rmiUsersManager;
	private VerifyCertificate verifyCertificate;
	
	public UserCertificateManager(AuthentificationManagerConfig authentificationManagerConfig, EncryptionManager encryptionManager, GeneralCertificateManager generalCertificateManager) throws RemoteException, InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, JSONException, IllegalBlockSizeException, BadPaddingException, WrongCertificateException, CertificateExpiredException, BadCertificateException {
		super();
		this.authentificationManagerConfig = authentificationManagerConfig;
		this.encryptionManager = encryptionManager;
		this.generalCertificate = generalCertificateManager.getGeneralCertificate();
		this.verifyCertificate = new VerifyCertificate(generalCertificate);
	}

	@Override
	public void handle(HttpExchange t) throws IOException {
		boolean sendForm = false;
    	String login = null;
    	String password = null;
    	String error = "";
    	JSONObject userCertif = null;
		BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(t.getRequestBody(),"utf-8"));
        } catch(UnsupportedEncodingException e) {
            System.err.println("Erreur lors de la r�cup�ration du flux " + e);
            System.exit(-1);
        }
        String query = null;
        try {
            query = br.readLine();
        } catch(IOException e) {
            System.err.println("Erreur lors de la lecture d'une ligne " + e);
            System.exit(-1);
        }
        if(query != null) {
        	String[] args = query.split("&");
        	for (int i = 0; i < args.length; i++) {
				String[] q2 = args[i].split("=");
				if(q2.length < 2)
					continue;
				if(URLDecoder.decode(q2[0],"ISO-8859-1").equals("email"))
					login = URLDecoder.decode(q2[1],"ISO-8859-1");
				if(URLDecoder.decode(q2[0],"ISO-8859-1").equals("password"))
					password = URLDecoder.decode(q2[1],"ISO-8859-1");
			}
        	if(login != null && !login.equals("") && password != null && !password.equals(""))
        		sendForm = true;
        }
        if(sendForm) {
        	userCertif = new JSONObject(getUserCertificate(login, password));
        	if(userCertif.has("Error")) {
        		Logger.log("User "+t.getRemoteAddress()+" tried to connect with login "+login+" and failed.", Type.UserCommand);
        		sendForm = false;
        		error = userCertif.getString("Error");
        	}else {
        		Logger.log("User "+t.getRemoteAddress()+" tried to connect with login "+login+" and succeed.", Type.UserCommand);
        	}
        }
        if(!sendForm) {
        	String answer = "<!DOCTYPE html>\r\n" + 
        			"<html>\r\n" + 
        			"<head>\r\n" + 
        			"<title>Login to get User Certificate</title>\r\n" + 
        			"</head>\r\n" + 
        			"<body>\r\n" + 
        			"<form action=\"login.jeej\" method=\"post\">\r\n" + 
        			"<p>Login to get User Certificate :</p>"+
        			"<p>Email : <input type=\"text\" name=\"email\" /></p>\r\n" + 
        			"<p>Password : <input type=\"password\" name=\"password\" /></p>\r\n" + 
        			"<p><input type=\"submit\" value=\"Get Certificate\"></p>\r\n" +
        			"<p>"+error+"</p>"+
        			"</form>\r\n" + 
        			"</body>\r\n" + 
        			"</html>";
		    try {
		        Headers h = t.getResponseHeaders();
		        h.set("Content-Type", "text/html; charset=utf-8");
		        t.sendResponseHeaders(200, answer.getBytes("UTF-8").length);
		    } catch(IOException e) {
		        System.err.println("Erreur lors de l'envoi de l'en-t�te : " + e);
		        System.exit(-1);
		    }
		    try {
		        OutputStream os = t.getResponseBody();
		        os.write(answer.getBytes("UTF-8"));
		        os.close();
		    } catch(IOException e) {
		        System.err.println("Erreur lors de l'envoi du corps : " + e);
		    }
        }else {
        	String ans = userCertif.toString(3);
		    try {
		        Headers h = t.getResponseHeaders();
		        h.set("Content-Type", "text/plain; charset=utf-8");
		        h.set("Content-Disposition", "attachment; filename=\"userCertificate.json\"");
		        t.sendResponseHeaders(200, ans.getBytes("UTF-8").length);
		    } catch(IOException e) {
		        System.err.println("Erreur lors de l'envoi de l'en-t�te : " + e);
		        System.exit(-1);
		    }
		    try {
		        OutputStream os = t.getResponseBody();
		        os.write(ans.getBytes("UTF-8"));
		        os.close();
		    } catch(IOException e) {
		        System.err.println("Erreur lors de l'envoi du corps : " + e);
		    }
        }
	}

	public String getUserCertificate(String login, String password) {
		if(rmiUsersManager == null) {
			try {
				rmiUsersManager = (RmiUsersManager) Naming.lookup(authentificationManagerConfig.getUserManagerRMIObjectUrl());
				String serverCertificate = rmiUsersManager.getCertificate();
				try {
					verifyCertificate.verifyCertificate(serverCertificate, "Server");
				} catch (Exception e) {
					System.err.println("Error while getting server certificate");
					e.printStackTrace();
					JSONObject o = new JSONObject();
					o.put("Error", "Internal Server Error");
					return o.toString();
				}
				
			} catch (MalformedURLException | RemoteException | NotBoundException e) {
				System.err.println("Error while trying to get rmiUsersManager on login demand");
				e.printStackTrace();
				JSONObject o = new JSONObject();
				o.put("Error", "Internal Server Error");
				return o.toString();
			}
		}
		
		
		JSONObject dem = new JSONObject();
		dem.put("Email", login);
		String ansenc = null;
		try {
			ansenc = rmiUsersManager.getUserId(generalCertificate, dem.toString());
		} catch (RemoteException e) {
			System.err.println("Error while getting userid");
			e.printStackTrace();
			JSONObject o = new JSONObject();
			o.put("Error", "Internal Server Error");
			return o.toString();
		}
		if(ansenc == null) {
			System.err.println("Error while getting userid internal on usersManager");
			JSONObject o = new JSONObject();
			o.put("Error", "Internal Server Error");
			return o.toString();
		}
		
		JSONObject ansjson = new JSONObject(ansenc);
		
		
		if(ansjson.has("Error")) {
			JSONObject o = new JSONObject();
			o.put("Error", "Unknown");
			return o.toString();
		}
		long id = ansjson.getLong("Id");
		String salt = ansjson.getString("Salt");
		
		String hashedPassword = encryptionManager.getSha().hash(password, salt);
		dem = new JSONObject();
		dem.put("HashedPassword", hashedPassword);
		dem.put("Id", id);
		
		ansenc = null;
		try {
			ansenc = rmiUsersManager.getInformation(generalCertificate, dem.toString());
		} catch (RemoteException e) {
			System.err.println("Error while getting userid");
			e.printStackTrace();
			JSONObject o = new JSONObject();
			o.put("Error", "Internal Server Error");
			return o.toString();
		}
		if(ansenc == null) {
			System.err.println("Error while getting userid internal on usersManager");
			JSONObject o = new JSONObject();
			o.put("Error", "Internal Server Error");
			return o.toString();
		}
		
		JSONObject certif = new JSONObject(ansenc);

		
		if(certif.has("Error")) {
			JSONObject o = new JSONObject();
			o.put("Error", "Unknown");
			return o.toString();
		}
		ansjson.toString();
		JSONObject file = new JSONObject();
		//Contenue Certificat
		certif.put("Type", "User");
		certif.put("Validity", LocalDateTime.now().plusSeconds(authentificationManagerConfig.getUserCertificateValidity()));
		//Fin Certificat
		String certifstring = certif.toString(3);
		String sha = encryptionManager.getSha().hash(certifstring);
		file.put("Certificate", certifstring);
		try {
			file.put("Signature", encryptionManager.getRsa().encryptWithPrivate(sha));
			return file.toString(3);
		} catch (JSONException | IllegalBlockSizeException | BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			JSONObject o = new JSONObject();
			o.put("Error", "Internal Server Error");
			return o.toString();
		}
	}

}
