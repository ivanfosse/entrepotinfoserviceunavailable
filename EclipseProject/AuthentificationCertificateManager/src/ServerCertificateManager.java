import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;

import org.json.JSONException;
import org.json.JSONObject;

import rmiIterface.RmiServerCertificateManager;
import util.EncryptionManager;

public class ServerCertificateManager extends UnicastRemoteObject implements RmiServerCertificateManager{
	private static final long serialVersionUID = -7826387307971180969L;
	private EncryptionManager encryptionManager;
	
	public ServerCertificateManager(EncryptionManager encryptionManager) throws RemoteException{
		this.encryptionManager = encryptionManager;
	}

	@Override
	public String getServerCertificate(String serverName) throws RemoteException {
			try {
				JSONObject file = new JSONObject();
				JSONObject certif = new JSONObject();
				//Contenue Certificat
				certif.put("Type", "Server");
				certif.put("ServerName", serverName);
				//Fin Certificat
				String certifstring = certif.toString(3);
				String sha = encryptionManager.getSha().hash(certifstring);
				file.put("Certificate", certifstring);
				file.put("Signature", encryptionManager.getRsa().encryptWithPrivate(sha));
				return file.toString(3);
			} catch (JSONException | IllegalBlockSizeException | BadPaddingException e) {
				e.printStackTrace();
			}
		return null;
	}
}
