import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;

import org.json.JSONException;
import org.json.JSONObject;

import rmiIterface.RmiWarehouseCertificateManager;
import util.EncryptionManager;
import util.VerifyCertificate;

public class WarehouseCertificateManager extends UnicastRemoteObject implements RmiWarehouseCertificateManager{
	private static final long serialVersionUID = 6400282437699277219L;
	private EncryptionManager encryptionManager;
	private VerifyCertificate verifyCertificate;
	
	public WarehouseCertificateManager(EncryptionManager encryptionManager, VerifyCertificate verifyCertificate) throws RemoteException{
		this.encryptionManager = encryptionManager;
		this.verifyCertificate = verifyCertificate;
	}
	

	@Override
	public String getWarehouseCertificate(String warehouseName) throws RemoteException {
			try {
				JSONObject file = new JSONObject();
				JSONObject certif = new JSONObject();
				//Contenue Certificat
				certif.put("Type", "Warehouse");
				certif.put("WarehouseName", warehouseName);
				//Fin Certificat
				String certifstring = certif.toString(3);
				String sha = encryptionManager.getSha().hash(certifstring);
				file.put("Certificate", certifstring);
				file.put("Signature", encryptionManager.getRsa().encryptWithPrivate(sha));
				return file.toString(3);
			} catch (JSONException | IllegalBlockSizeException | BadPaddingException e) {
				e.printStackTrace();
			}
		return null;
	}
}
