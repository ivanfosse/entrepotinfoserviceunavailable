import util.Config;

public class AuthentificationManagerConfig {
	private String UserManagerRMIObjectUrl;
	private String GeneralCertificateManagerRMIObjectUrl;
	private String ServerCertificateManagerRMIObjectUrl;
	private String WarehouseCertificateManagerRMIObjectUrl;
	private int RSAKeySize;
	private long UserCertificateValidity;
	private int HttpServerPort;
	
	public AuthentificationManagerConfig() {
		Config config = new Config("./configs/AuthentificationManagerConfig.json");
		UserManagerRMIObjectUrl = config.get("UsersManagerRMIObjectUrl", "rmi://localhost:28000/UsersManager");
		GeneralCertificateManagerRMIObjectUrl = config.get("GeneralCertificateManagerRMIObjectUrl","rmi://localhost:28000/GeneralCertificateManager");
		ServerCertificateManagerRMIObjectUrl = config.get("ServerCertificateManagerRMIObjectUrl","rmi://localhost:28000/ServerCertificateManager");
		WarehouseCertificateManagerRMIObjectUrl = config.get("WarehouseCertificateManagerRMIObjectUrl","rmi://localhost:28000/WarehouseCertificateManager");
		RSAKeySize = Integer.parseInt(config.get("RSAKeySize", "2048"));
		UserCertificateValidity = Long.parseLong(config.get("UserCertificateValidity", "3600"));
		HttpServerPort = Integer.parseInt(config.get("HttpServerPort","8080"));
	}

	public String getUserManagerRMIObjectUrl() {
		return UserManagerRMIObjectUrl;
	}

	public String getGeneralCertificateManagerRMIObjectUrl() {
		return GeneralCertificateManagerRMIObjectUrl;
	}

	public int getRSAKeySize() {
		return RSAKeySize;
	}

	public long getUserCertificateValidity() {
		return UserCertificateValidity;
	}

	public String getServerCertificateManagerRMIObjectUrl() {
		return ServerCertificateManagerRMIObjectUrl;
	}

	public int getHttpServerPort() {
		return HttpServerPort;
	}

	public String getWarehouseCertificateManagerRMIObjectUrl() {
		return WarehouseCertificateManagerRMIObjectUrl;
	}
	
	
	
	
}
