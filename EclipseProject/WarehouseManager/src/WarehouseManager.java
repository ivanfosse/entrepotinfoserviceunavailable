import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Random;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import rmiIterface.RmiGeneralCertificateManager;
import rmiIterface.RmiServerCertificateManager;
import rmiIterface.RmiWarehouseCertificateManager;
import rmiIterface.RmiWarehouseManager;
import util.Command;
import util.Console;
import util.Logger;
import util.VerifyCertificate;
import util.exception.BadCertificateException;
import util.exception.CertificateExpiredException;
import util.exception.WrongCertificateException;

public class WarehouseManager extends UnicastRemoteObject implements RmiWarehouseManager {
	private static final long serialVersionUID = 1753260919653073652L;
	private WarehouseManagerConfig warehouseManagerConfig;
	private Console console;
	
	private String generalCertificate;
	private VerifyCertificate verifyCertificate;
	private String serverCertificate;
	
	private HashMap<Long, Owner> knowedOwners;
	private HashMap<String, WarehouseContainer> warehouses;
	private HashSet<Integer> IdAllocated;
	
	
	

	public WarehouseManager() throws RemoteException{
		knowedOwners = new HashMap<>();
		warehouses = new HashMap<>();
		warehouseManagerConfig = new WarehouseManagerConfig();
		IdAllocated = new HashSet<>();
		
		
		System.out.println("Getting General Certificate");
		try {
			RmiGeneralCertificateManager rmiGeneralCertificateManager = (RmiGeneralCertificateManager)Naming.lookup(warehouseManagerConfig.getGeneralCertificateManagerRMIObjectUrl());
			generalCertificate = rmiGeneralCertificateManager.getGeneralCertificate();
			verifyCertificate = new VerifyCertificate(rmiGeneralCertificateManager.getGeneralCertificate());
		} catch (MalformedURLException | RemoteException | NotBoundException | InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException | JSONException | IllegalBlockSizeException | BadPaddingException | WrongCertificateException | CertificateExpiredException | BadCertificateException e) {
			System.err.println("Cannot get general certificate shuting down");
			e.printStackTrace();
			System.exit(1);
		}
		System.out.println("Getting Server Certificate");
		try {
			serverCertificate = ((RmiServerCertificateManager)Naming.lookup(warehouseManagerConfig.getServerCertificateManagerRMIObjectUrl())).getServerCertificate("WarehouseManager");
			verifyCertificate.verifyCertificate(serverCertificate, "Server");
		} catch (IllegalBlockSizeException | BadPaddingException | JSONException 
				| BadCertificateException | CertificateExpiredException | MalformedURLException | NotBoundException e) {
			System.err.println("Cannot get server certificate shuting down");
			e.printStackTrace();
			System.exit(1);
		}
		try {
			Naming.rebind(warehouseManagerConfig.getWarehouseManagerRMIObjectUrl(), this);
		} catch (MalformedURLException | RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
		try {
			System.out.println("Loading Owners");
			loadAllOwner();
			System.out.println("Loading Warehouses");
			loadAllWareHouse();
			System.out.println("Loading ID");
			loadID();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		System.out.println(warehouses.size());
		
		initConsole();
	}
	
	@Override
	protected void finalize() throws Throwable {
		for(Entry<String, WarehouseContainer> e:warehouses.entrySet()) {
			e.getValue().stopProcess();
		}
		super.finalize();
	}
	
	public void saveWareHouse(WarehouseContainer warehouseContainer) throws IOException {
		JSONObject json = warehouseContainer.toJSON();
		BufferedWriter bw = new BufferedWriter(new FileWriter(new File(warehouseManagerConfig.getWarehousesLocation()+"/"+warehouseContainer.getName()+"warehouse.json")));
		bw.write(json.toString(1));
		bw.flush();
		bw.close();
		bw = null;
	}
	
	public void saveOwner(Owner owner) throws IOException {
		JSONObject json = owner.toJSON();
		BufferedWriter bw = new BufferedWriter(new FileWriter(new File(warehouseManagerConfig.getOwnersLocation()+"/"+owner.getGROUP_ID()+"owner.json")));
		bw.write(json.toString(1));
		bw.flush();
		bw.close();
		bw = null;
	}
	
	public void loadAllOwner() throws IOException {
		knowedOwners.clear();
		Files.createDirectories(Paths.get(warehouseManagerConfig.getOwnersLocation()));
		File folder = new File(warehouseManagerConfig.getOwnersLocation());
		for (File file : folder.listFiles()) {
			String str = new String(Files.readAllBytes(file.toPath()));
			Owner owner = new Owner(new JSONObject(str), this);
		}
		Owner owner = new Owner(0, warehouseManagerConfig.getOwnerWarehouseManagerName(), warehouseManagerConfig.getOwnerWarehouseManagerAdress(), this);
		System.out.println(knowedOwners.get(0L));
	}
	
	public void loadAllWareHouse() throws IOException {
		warehouses.clear();
		Files.createDirectories(Paths.get(warehouseManagerConfig.getWarehousesLocation()));
		File folder = new File(warehouseManagerConfig.getWarehousesLocation());
		for (File file : folder.listFiles()) {
			String str = new String(Files.readAllBytes(file.toPath()));
			WarehouseContainer warehouseContainer;
			System.out.println(file.getName());
			try {
				warehouseContainer = new WarehouseContainer(new JSONObject(str), this,(RmiWarehouseCertificateManager)Naming.lookup(warehouseManagerConfig.getWarehouseCertificateManagerRMIObjectUrl()), generalCertificate, serverCertificate);
				warehouses.put(warehouseContainer.getName(), warehouseContainer);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void saveID() throws IOException {
		JSONArray j = new JSONArray(IdAllocated);
		BufferedWriter bw = new BufferedWriter(new FileWriter(new File("./configs/idAllocated.json")));
		bw.write(j.toString());
		bw.flush();
		bw.close();
		bw = null;
	}
	
	public void loadID() throws IOException {
		if(new File("./configs/idAllocated.json").exists()) {
			String str = new String(Files.readAllBytes(new File("./configs/idAllocated.json").toPath()));
			JSONArray j = new JSONArray(str);
			for (int i = 0; i < j.length(); i++) {
				IdAllocated.add(j.getInt(i));
			}
		}
	}
	
	private void initConsole(){
		console = new Console();
		console.addCommand("createwarehouse", new Command() {
			
			@Override
			public void execute(String[] args) {
				if(args.length != 4) {
					System.out.println("Wrong args: createwarehouse name adresse port");
					return;
				}
				String name = args[1];
				String adress = args[2];
				int port = Integer.parseInt(args[3]);
				if(!name.equals(name.replaceAll("[^A-Za-z0-9]", ""))) {
					System.out.println("Wrong args: name need to be alphanumeric only");
					return;
				}
				try {
					System.out.println("Creating new warehouse :"+name);
					WarehouseContainer warehouseContainer = new WarehouseContainer(name,adress,knowedOwners.get(0L),port,(RmiWarehouseCertificateManager)Naming.lookup(warehouseManagerConfig.getWarehouseCertificateManagerRMIObjectUrl()), generalCertificate, serverCertificate);
					warehouses.put(warehouseContainer.getName(), warehouseContainer);
					saveWareHouse(warehouseContainer);
					System.out.println("Creation successful !");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		console.addCommand("createowner", new Command() {
			
			@Override
			public void execute(String[] args) {
				if(args.length != 4) {
					System.out.println("Wrong args: createowner id name adresse");
					return;
				}
				long id = Long.parseLong(args[1]);
				String name = args[2];
				String adress = args[3];
				try {
					System.out.println("Creating new owner :"+name);
					Owner o = new Owner(id, name, adress, WarehouseManager.this);
					saveOwner(o);
					System.out.println("Creation successful !");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		console.addCommand("stop", new Command() {
			
			@Override
			public void execute(String[] args) {
				for(Entry<String, WarehouseContainer> e:warehouses.entrySet()) {
					e.getValue().stopProcess();
				}
				console.stop();
			}
		});
		
		console.launch();
		System.exit(0);
	}
	
	public Owner updateKnowedOwners( Owner owner )
	{
		Owner o = knowedOwners.get( owner.getGROUP_ID() );

		if( o == null )
		{
			o = owner;
			knowedOwners.put( o.getGROUP_ID(), o );
		}else {
			o.setAdress(owner.getAdress());
			o.setName(owner.getName());
		}

		return o;
	}
	
	
	public static void main(String[] args) throws RemoteException {
		System.runFinalizersOnExit(true);
		Logger.initialiseStream();
		WarehouseManager auth = new WarehouseManager();
	}

	@Override
	public String executeCommand(String certificate, String args) throws RemoteException {
		System.out.println(certificate);
		System.out.println(args);
		JSONObject answerjson = new JSONObject();
		try {
			verifyCertificate.verifyCertificate(certificate, "Server");
		} catch (IllegalBlockSizeException | BadPaddingException | JSONException | BadCertificateException
				| CertificateExpiredException e1) {
			answerjson.put("Answer", "Error : Unknown Certificate");
		}
		System.out.println("HEEEEEEE");
		JSONObject argsjson = new JSONObject(args);
		String type = argsjson.getString("Type");
		int continuet = 0;
		if(	type.equals("AddContainer") || 
			type.equals("ModifyContainer")||
			type.equals("RemoveContainer")||
			type.equals("GetContainer")||
			type.equals("MoveContainer")||
			type.equals("WarehouseInfo"))
			continuet = 1;
		if(	type.equals("WarehouseList"))
				continuet = 2;
		if(argsjson.has("UserOwnerId")) {
			Owner o = knowedOwners.get(argsjson.getLong("UserOwnerId"));
			if(o == null) {
				o = new Owner(argsjson.getLong("UserOwnerId"), "Unknown", "Unknown", this);
			}
			argsjson.put("Owner", o.toJSON());
		}
		if(type.equals(continuet))
			continuet = 2;
		System.out.println(continuet);
		System.out.println(argsjson.toString());
		if(continuet == 0) {
			answerjson.put("Answer", "Error : Unknown Command");
		}else if(continuet == 1){

			System.out.println("AAA");
			if(argsjson.has("Warehouse")) {
				String wh = argsjson.getString("Warehouse");
				WarehouseContainer warehouseContainer = warehouses.get(wh);
				System.out.println(warehouseContainer);
				if(warehouseContainer != null) {
					System.out.println("BBB");
					int i = -1;
					if(type.equals("AddContainer")) {
						System.out.println("CCC");
						while(i < 0) {
							i = Math.abs(new Random().nextInt());
							if(!IdAllocated.contains(new Integer(i))) {
								IdAllocated.add(i);
							}else {
								i = -1;
							}
						}
						System.out.println("DDD");
						try {
							saveID();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						argsjson.put("ID", i);
					}
					if(type.equals("MoveContainer")) {
						if(argsjson.has("WarehouseDestination")) {
							String str = argsjson.getString("WarehouseDestination");
							WarehouseContainer warehouseContainerDest = warehouses.get(str);
							if(warehouseContainerDest == null) {
								answerjson.put("Answer", "Error : Unknown Warehouse Destination");
								return answerjson.toString();
							}
							argsjson.put("Port",warehouseContainerDest.getPort());
							argsjson.put("IpAdress","localhost");
						}else {
							answerjson.put("Answer", "Error : Need a Destination");
							return answerjson.toString();
						}
					}
					System.out.println(argsjson.toString());
					try {
						System.out.println("SENDING TO WH");
						String rc = warehouseContainer.sendAndReceiveMessage(argsjson.toString());
						if(rc != null) {
							answerjson = new JSONObject(rc);
						}else {
							answerjson = new JSONObject();
							answerjson.put("Answer", "Error : Cannot Contact Warehouse or send command");
						}
					} catch (IOException e) {
						answerjson.put("Answer", "Error : Cannot Contact Warehouse");
						e.printStackTrace();
					}
				}else {
					answerjson.put("Answer", "Error : Unknown Warehouse");
				}
			}else {
				answerjson.put("Answer", "Error : Need Warehouse");
			}
			
		}else if(continuet == 2){
			answerjson.put("List",new JSONArray(warehouses.keySet()));
		}
		
		return answerjson.toString();
	}

	@Override
	public String getCertificate() throws RemoteException {
		// TODO Auto-generated method stub
		return serverCertificate;
	}
}
