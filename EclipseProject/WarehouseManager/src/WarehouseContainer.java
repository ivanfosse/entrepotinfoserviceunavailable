import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.charset.Charset;

import org.json.JSONObject;

import rmiIterface.RmiWarehouseCertificateManager;

public class WarehouseContainer {
	private Process processus;
	private String name;
	private String adresse;
	private Owner owner;
	private int port;
	
	public WarehouseContainer(String name, String adresse, Owner owner, int port, RmiWarehouseCertificateManager rmiWarehouseCertificateManager, String generalCertificate, String serverCertificate) throws Exception {
		this.name = name;
		this.adresse = adresse;
		this.owner = owner;
		this.port = port;
		
        JSONObject j = new JSONObject();
        j.put("GeneralCertificate", generalCertificate);
        j.put("ServerCertificate", serverCertificate);
        j.put("WarehouseCertificate", rmiWarehouseCertificateManager.getWarehouseCertificate(name));
        j.put("Type", "Initialise");
        j.put("Adresse", adresse);
        j.put("Owner", owner.toJSON());
        String receive = this.sendAndReceiveMessage(j.toString());
        System.out.println(receive);
        if(!new JSONObject(receive).getString("Result").equals("Ok"))
        	throw new Exception("Cannot turn on warehouse "+name);
	}
	
	public WarehouseContainer(JSONObject json, WarehouseManager warehouseManager, RmiWarehouseCertificateManager rmiWarehouseCertificateManager, String generalCertificate, String serverCertificate) throws Exception {
		this(json.getString("name"), json.getString("adresse"), new Owner(json.getJSONObject("owner"), warehouseManager), json.getInt("port"), rmiWarehouseCertificateManager, generalCertificate, serverCertificate);
	}
	
	public synchronized String sendAndReceiveMessage(String message) throws IOException {
        byte[] receive = new byte[64000];
        DatagramPacket receivedp = new DatagramPacket(receive, 64000);
		DatagramSocket server = new DatagramSocket();
		Charset charset;
		if (Charset.isSupported("UTF-8"))
			charset = Charset.forName("UTF-8");
		else {
			charset = Charset.defaultCharset();
			System.out.println("Unsuported UTF-8 charged default charset :" + charset.name());
		}
		DatagramPacket msg = null;
        byte[] tampon = message.getBytes(charset);
		try {
			msg = new DatagramPacket(tampon, tampon.length, InetAddress.getByName("localhost"), port);
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        int tryy = 5;
        while(tryy != 0) {
    		server.send(msg);
    		try {
    			server.setSoTimeout(5000); 
    			server.receive(receivedp);
    			break;
	        }catch(SocketTimeoutException e) {
	        	tryy--;
	        }
        }
        if(tryy == 0) {
        	return null;
        }
        return new String(receivedp.getData(), 0, receivedp.getLength(), charset);
	}
	
	public void stopProcess() {
		processus.destroy();
	}

	
	public String getName() {
		return name;
	}

	public String getAdresse() {
		return adresse;
	}

	public Owner getOwner() {
		return owner;
	}

	public int getPort() {
		return port;
	}
	
	public JSONObject toJSON() {
		return new JSONObject(this);
	}
	
}
