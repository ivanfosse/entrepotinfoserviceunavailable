import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import util.Config;

public class WarehouseManagerConfig {
	private String GeneralCertificateManagerRMIObjectUrl;
	private String ServerCertificateManagerRMIObjectUrl;
	private String WarehouseCertificateManagerRMIObjectUrl;
	private String WarehouseManagerRMIObjectUrl;
	private String WarehouseJarLocation;
	private String OwnersLocation;
	private String OwnerWarehouseManagerName;
	private String OwnerWarehouseManagerAdress;
	private String WarehousesLocation;
	private int WarehousesPortLocation;
	private int RSAKeySize;
	
	public WarehouseManagerConfig() {
		Config config = new Config("./configs/WarehouseManagerConfig.json");
		GeneralCertificateManagerRMIObjectUrl = config.get("GeneralCertificateManagerRMIObjectUrl","rmi://localhost:28000/GeneralCertificateManager");
		ServerCertificateManagerRMIObjectUrl = config.get("ServerCertificateManagerRMIObjectUrl","rmi://localhost:28000/ServerCertificateManager");
		WarehouseCertificateManagerRMIObjectUrl = config.get("WarehouseCertificateManagerRMIObjectUrl","rmi://localhost:28000/WarehouseCertificateManager");
		WarehouseManagerRMIObjectUrl = config.get("WarehouseManagerRMIObjectUrl","rmi://localhost:28000/WarehouseManager");
		WarehouseJarLocation = config.get("WarehouseJarLocation","Warehouse.jar");
		OwnersLocation = config.get("OwnersLocation","./owners");
		try {
			Files.createDirectories(Paths.get(OwnersLocation));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (OwnersLocation.charAt(OwnersLocation.length() - 1) == '/' || OwnersLocation.charAt(OwnersLocation.length() - 1) == '\\')
			OwnersLocation = OwnersLocation.substring(0, OwnersLocation.length() - 1);
		WarehousesLocation = config.get("WarehousesLocation","./warehouseservers");
		try {
			Files.createDirectories(Paths.get(WarehousesLocation));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (WarehousesLocation.charAt(WarehousesLocation.length() - 1) == '/' || WarehousesLocation.charAt(WarehousesLocation.length() - 1) == '\\')
			WarehousesLocation = WarehousesLocation.substring(0, WarehousesLocation.length() - 1);
		RSAKeySize = Integer.parseInt(config.get("RSAKeySize", "2048"));
		OwnerWarehouseManagerName = config.get("OwnerWarehouseManagerName","OwnerWarehouseManagerName");
		OwnerWarehouseManagerAdress = config.get("OwnerWarehouseManagerAdress","OwnerWarehouseManagerAdress");
		WarehousesPortLocation = Integer.parseInt(config.get("WarehousesPortLocation","27000"));
	}


	public String getGeneralCertificateManagerRMIObjectUrl() {
		return GeneralCertificateManagerRMIObjectUrl;
	}

	public int getRSAKeySize() {
		return RSAKeySize;
	}

	public String getServerCertificateManagerRMIObjectUrl() {
		return ServerCertificateManagerRMIObjectUrl;
	}

	public String getWarehouseCertificateManagerRMIObjectUrl() {
		return WarehouseCertificateManagerRMIObjectUrl;
	}


	public String getWarehouseJarLocation() {
		return WarehouseJarLocation;
	}

	public String getOwnersLocation() {
		return OwnersLocation;
	}


	public String getOwnerWarehouseManagerName() {
		return OwnerWarehouseManagerName;
	}


	public String getOwnerWarehouseManagerAdress() {
		return OwnerWarehouseManagerAdress;
	}


	public String getWarehousesLocation() {
		return WarehousesLocation;
	}


	public int getWarehousesPortLocation() {
		return WarehousesPortLocation;
	}


	public String getWarehouseManagerRMIObjectUrl() {
		return WarehouseManagerRMIObjectUrl;
	}
	
	
}
