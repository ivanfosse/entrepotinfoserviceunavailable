import util.Config;

public class UsersManagerConfig {
	private String UserManagerRMIObjectUrl;
	private String ServerCertificateManagerRMIObjectUrl;
	private String GeneralCertificateManagerRMIObjectUrl;
	private String UsersFolder;
	private int RSAKeySize;
	
	public UsersManagerConfig() {
		Config config = new Config("./configs/UsersManagerConfig.json");
		UserManagerRMIObjectUrl = config.get("UsersManagerRMIObjectUrl", "rmi://localhost:28000/UsersManager");
		ServerCertificateManagerRMIObjectUrl = config.get("ServerCertificateManagerRMIObjectUrl","rmi://localhost:28000/ServerCertificateManager");
		GeneralCertificateManagerRMIObjectUrl = config.get("GeneralCertificateManagerRMIObjectUrl","rmi://localhost:28000/GeneralCertificateManager");
		UsersFolder = config.get("UsersFolder","./users");
		if (UsersFolder.charAt(UsersFolder.length() - 1) == '/' || UsersFolder.charAt(UsersFolder.length() - 1) == '\\')
			UsersFolder = UsersFolder.substring(0, UsersFolder.length() - 1);
		RSAKeySize = Integer.parseInt(config.get("RSAKeySize", "2048"));
	}

	public String getUserManagerRMIObjectUrl() {
		return UserManagerRMIObjectUrl;
	}

	public String getServerCertificateManagerRMIObjectUrl() {
		return ServerCertificateManagerRMIObjectUrl;
	}

	public int getRSAKeySize() {
		return RSAKeySize;
	}

	public String getGeneralCertificateManagerRMIObjectUrl() {
		return GeneralCertificateManagerRMIObjectUrl;
	}

	public String getUsersFolder() {
		return UsersFolder;
	}
	
	

}
