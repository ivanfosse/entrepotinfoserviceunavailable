import org.json.JSONObject;

public class User {
	private static long idInc;
	private final long ID;
	private String email;
	private String hashedPassword;
	private String salt;
	private String role;
	private long groupId;
	
	public User(JSONObject json) {
		ID = json.getLong("Id");
		if(ID>=idInc)
			idInc = ID+1;
		this.email = json.getString("Email");
		this.hashedPassword = json.getString("HashedPassword");
		this.salt = json.getString("Salt");
		this.role = json.getString("Role");
		this.groupId = json.getLong("GroupId");;
		
	}
	
	public JSONObject tojson(){
		JSONObject json = new JSONObject();
		json.put("Id", ID);
		json.put("Email", email);
		json.put("HashedPassword", hashedPassword);
		json.put("Salt", salt);
		json.put("Role", role);
		json.put("GroupId", groupId);
		return json;
	}

	public User(String email, String hashedPassword, String salt, String role, long groupId) {
		this.ID = idInc++;
		this.email = email;
		this.hashedPassword = hashedPassword;
		this.salt = salt;
		this.role = role;
		this.groupId = groupId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (ID ^ (ID >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (ID != other.ID)
			return false;
		return true;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getHashedPassword() {
		return hashedPassword;
	}

	public void setHashedPassword(String hashedPassword) {
		this.hashedPassword = hashedPassword;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public long getGroupId() {
		return groupId;
	}

	public void setGroupId(long groupId) {
		this.groupId = groupId;
	}

	public long getID() {
		return ID;
	}
	
	
}
