import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Random;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.json.JSONException;
import org.json.JSONObject;

import rmiIterface.RmiGeneralCertificateManager;
import rmiIterface.RmiServerCertificateManager;
import rmiIterface.RmiUsersManager;
import util.Command;
import util.Console;
import util.Logger;
import util.ShaEncrypt;
import util.ShaEncrypt.TypeEncrypt;
import util.VerifyCertificate;
import util.exception.BadCertificateException;
import util.exception.CertificateExpiredException;
import util.exception.WrongCertificateException;

public class UsersManager extends UnicastRemoteObject implements RmiUsersManager{
	UsersManagerConfig usersManagerConfig;
	VerifyCertificate verifyCertificate;
	
	String serverCertificate;
	
	HashMap<Long, User> usersById;
	HashMap<String, User> usersByEmail;
	
	Console console;
	
	public UsersManager() throws RemoteException{
		usersManagerConfig = new UsersManagerConfig();
		usersById = new HashMap<>();
		usersByEmail = new HashMap<>();

		System.out.println("Loading Users");
		try {
			loadAll();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		System.out.println("Getting General Certificate");
		try {
			RmiGeneralCertificateManager rmiGeneralCertificateManager = (RmiGeneralCertificateManager)Naming.lookup(usersManagerConfig.getGeneralCertificateManagerRMIObjectUrl());
			verifyCertificate = new VerifyCertificate(rmiGeneralCertificateManager.getGeneralCertificate());
		} catch (MalformedURLException | RemoteException | NotBoundException | InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException | JSONException | IllegalBlockSizeException | BadPaddingException | WrongCertificateException | CertificateExpiredException | BadCertificateException e) {
			System.err.println("Cannot get general certificate shuting down");
			e.printStackTrace();
			System.exit(1);
		}
		System.out.println("Getting Server Certificate");
		try {
			serverCertificate = ((RmiServerCertificateManager)Naming.lookup(usersManagerConfig.getServerCertificateManagerRMIObjectUrl())).getServerCertificate("UsersManager");
			verifyCertificate.verifyCertificate(serverCertificate);
		} catch (IllegalBlockSizeException | BadPaddingException | JSONException 
				 | BadCertificateException | CertificateExpiredException | MalformedURLException | NotBoundException e) {
			System.err.println("Cannot get server certificate shuting down");
			e.printStackTrace();
			System.exit(1);
		}
		try {
			Naming.rebind(usersManagerConfig.getUserManagerRMIObjectUrl(), this);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		initConsole();
	}
	
	public void initConsole() {
		console = new Console();
		
		console.addCommand("CreateUser", new Command() {
			
			@Override
			public void execute(String[] args) {
				if(args.length != 5) {
					System.out.println("Wrong args: createuser email password role groupid");
					return;
				}
				final Random r = new SecureRandom();
				byte[] salt = new byte[32];
				r.nextBytes(salt);
				String encodedSalt = Base64.getEncoder().encodeToString(salt);
				String password = null;
				try {
					password = new ShaEncrypt(TypeEncrypt.SHA512).hash(args[2], encodedSalt);
				} catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return;
				}
				String email = args[1];
				if(usersByEmail.containsKey(email)) {
					System.out.println("User with that email already exist !");
					return;
				}
				String role = args[3];
				long groupid = 0;
				try {
					groupid = Long.parseLong(args[4]);
				} catch (Exception e) {
					System.out.println("groupid need to be long");
					return;
				}
				if(role.equals("Admin") || role.equals("Manager") || role.equals("Client")) {
					User u = new User(email,password,encodedSalt,role,groupid);
					usersByEmail.put(u.getEmail(), u);
					usersById.put(u.getID(), u);
					try {
						saveUser(u);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else {
					System.out.println("Role need to be Admin or Manager or Client");
				}
			}
		});
		
		console.launch();
		System.exit(0);
	}
	
	public void loadAll() throws IOException {
		usersByEmail.clear();
		usersById.clear();
		Files.createDirectories(Paths.get(usersManagerConfig.getUsersFolder()));
		File folder = new File(usersManagerConfig.getUsersFolder());
		for (File file : folder.listFiles()) {
			String str = new String(Files.readAllBytes(file.toPath()));
			User user = new User(new JSONObject(str));
			usersById.put(user.getID(), user);
			usersByEmail.put(user.getEmail(), user);
		}
	}
	
	public void saveUser(User user) throws IOException {
		JSONObject json = user.tojson();
		BufferedWriter bw = new BufferedWriter(new FileWriter(new File(usersManagerConfig.getUsersFolder()+"/"+user.getID()+"user.json")));
		bw.write(json.toString(1));
		bw.flush();
		bw.close();
		bw = null;
	}
	

	public static void main(String[] args) throws RemoteException {
		Logger.initialiseStream();
		UsersManager UM = new UsersManager();
	}


	@Override
	public String getUserId(String certificate, String args) throws RemoteException{
		JSONObject jsonargs = new JSONObject(args);
		if(jsonargs == null)
			return null;
		
		User u = usersByEmail.get(jsonargs.getString("Email"));
		
		JSONObject jsonanswer = new JSONObject();
		if(u == null)
			jsonanswer.put("Error", "Unknown");
		else {
			jsonanswer.put("Id", u.getID());
			jsonanswer.put("Salt", u.getSalt());
		}
		
		return jsonanswer.toString();
	}

	@Override
	public String getCertificate() throws RemoteException{
		return serverCertificate;
	}

	@Override
	public String getInformation(String certificate, String args) throws RemoteException {
		JSONObject jsonargs = new JSONObject(args);
		if(jsonargs == null)
			return null;
		
		User u = usersById.get(jsonargs.getLong("Id"));
		String hsh = jsonargs.getString("HashedPassword");
		if(u == null)
			return null;
		JSONObject jsonanswer = null;
		if(!hsh.equals(u.getHashedPassword())){
			jsonanswer = new JSONObject();
			jsonanswer.put("Error", "Unknown");
		}
		else {
			jsonanswer = u.tojson();
			jsonanswer.remove("HashedPassword");
			jsonanswer.remove("Salt");
		}
		
		return jsonanswer.toString();
	}
}
