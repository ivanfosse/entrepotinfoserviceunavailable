package util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Locale;

public class DuplicateStream extends PrintStream {
	PrintStream ps;
	private boolean donot = false;

	public DuplicateStream(File file, PrintStream ps) throws FileNotFoundException {
		super(file);
		this.ps = ps;
	}

	@Override
	public PrintStream append(char c) {
		ps.append(c);
		return super.append(c);
	}

	@Override
	public PrintStream append(CharSequence csq, int start, int end) {
		ps.append(csq, start, end);
		return super.append(csq, start, end);
	}

	@Override
	public PrintStream append(CharSequence csq) {
		ps.append(csq);
		return super.append(csq);
	}

	@Override
	public void close() {
		ps.close();
		super.close();
	}

	@Override
	public void flush() {
		ps.flush();
		super.flush();
	}

	@Override
	public PrintStream format(Locale l, String format, Object... args) {
		ps.format(l, format, args);
		return super.format(l, format, args);
	}

	@Override
	public PrintStream format(String format, Object... args) {
		ps.format(format, args);
		return super.format(format, args);
	}

	@Override
	public void print(boolean b) {
		if (!donot)
			ps.print(b);
		super.print(b);
	}

	@Override
	public void print(char c) {
		if (!donot)
			ps.print(c);
		super.print(c);
	}

	@Override
	public void print(char[] s) {
		if (!donot)
			ps.print(s);
		super.print(s);
	}

	@Override
	public void print(double d) {
		if (!donot)
			ps.print(d);
		super.print(d);
	}

	@Override
	public void print(float f) {
		if (!donot)
			ps.print(f);
		super.print(f);
	}

	@Override
	public void print(int i) {
		if (!donot)
			ps.print(i);
		super.print(i);
	}

	@Override
	public void print(long l) {
		if (!donot)
			ps.print(l);
		super.print(l);
	}

	@Override
	public void print(Object obj) {
		if (!donot)
			ps.print(obj);
		super.print(obj);
	}

	@Override
	public void print(String s) {
		if (!donot)
			ps.print(s);
		super.print(s);
	}

	@Override
	public PrintStream printf(Locale l, String format, Object... args) {
		ps.printf(l, format, args);
		return super.printf(l, format, args);
	}

	@Override
	public PrintStream printf(String format, Object... args) {
		ps.printf(format, args);
		return super.printf(format, args);
	}

	@Override
	public void println() {
		donot = true;
		ps.println();
		super.println();
		donot = false;
	}

	@Override
	public void println(boolean x) {
		donot = true;
		ps.println(x);
		super.println(x);
		donot = false;
	}

	@Override
	public void println(char x) {
		donot = true;
		ps.println(x);
		super.println(x);
		donot = false;
	}

	@Override
	public void println(char[] x) {
		donot = true;
		ps.println(x);
		super.println(x);
		donot = false;
	}

	@Override
	public void println(double x) {
		donot = true;
		ps.println(x);
		super.println(x);
		donot = false;
	}

	@Override
	public void println(float x) {
		donot = true;
		ps.println(x);
		super.println(x);
		donot = false;
	}

	@Override
	public void println(int x) {
		donot = true;
		ps.println(x);
		super.println(x);
		donot = false;
	}

	@Override
	public void println(long x) {
		donot = true;
		ps.println(x);
		super.println(x);
		donot = false;
	}

	@Override
	public void println(Object x) {
		donot = true;
		ps.println(x);
		super.println(x);
		donot = false;
	}

	@Override
	public void println(String x) {
		donot = true;
		ps.println(x);
		super.println(x);
		donot = false;
	}

	/*
	 * @Override public void write(byte[] buf, int off, int len) { ps.write(buf,
	 * off, len); super.write(buf, off, len); }
	 * 
	 * @Override public void write(int b) { ps.write(b); super.write(b); }
	 */
}
