package util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

import rmiIterface.RmiLogger;

public class Logger {
	private static String logfolder;
	private static String progname;
	private static String LoggerRMIObjectURL;
	public static DuplicateStream out;
	public static DuplicateStream err;
	private static RmiLogger log;
	private static boolean tried = false;
	
	public static void initialiseStream() {
		if(logfolder == null) {
			Config config = new Config("./configs/LoggerConfig.json");
			LoggerRMIObjectURL = config.get("LoggerRMIObjectURL", "rmi://localhost:28000/Logger");
			logfolder = config.get("LogsFolder", "./logs");
			try {
				Files.createDirectories(Paths.get(logfolder));
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			if (logfolder.charAt(logfolder.length() - 1) == '/' || logfolder.charAt(logfolder.length() - 1) == '\\')
				logfolder = logfolder.substring(0, logfolder.length() - 1);
			StackTraceElement[] stack = Thread.currentThread ().getStackTrace ();
		    StackTraceElement main = stack[stack.length - 1];
		    progname = main.getClassName();
		    String date = LocalDateTime.now().format(DateTimeFormatter.ofPattern(("yyyy-MM-dd-HH-mm-ss")));
		    try {
		    	long id = Math.abs(new Random().nextInt());
				out = new DuplicateStream(new File(logfolder +"/"+ progname + "-"+id+"-LOG-" + date + ".logs"), System.out);
				err = new DuplicateStream(new File(logfolder +"/"+ progname + "-"+id+"-ERRLOG-" + date + ".logs"), System.err);
				System.setOut(out);
				System.setErr(err);
		    } catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public static void log(Object o, Type type) {
		if(log == null && !tried) {
			try {
				if(log == null)
					tried = true;
				log = (RmiLogger) Naming.lookup(LoggerRMIObjectURL);
				tried = false;
			} catch (MalformedURLException | RemoteException | NotBoundException e) {
				log = null;
			}
		}
		String res = "["+type.toString()+"]("+LocalDateTime.now().format(DateTimeFormatter.ofPattern(("yyyy-MM-dd-HH-mm-ss")))+") : "+o.toString();
		System.out.println("[LOG]"+res);
		if(log != null) {
			try {
				log.addLog(res, progname);
			} catch (RemoteException e) {
				System.err.println("Impossible d'envoyer une log");
				e.printStackTrace();
			}
		}
	}
	
	public enum Type {
		Error("ERROR"), 
		Warning("WARNING"), 
		Log("LOG"), 
		UserCommand("USERCOMMAND");

		private String name = "";

		private Type(String name) {
			this.name = name;
		}

		public String toString() {
			return name;
		}
	}
}
