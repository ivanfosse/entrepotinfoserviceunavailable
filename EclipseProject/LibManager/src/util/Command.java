package util;

public interface Command {
	public void execute(String[] args);
}
