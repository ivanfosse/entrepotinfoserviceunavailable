package util;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.NoSuchPaddingException;

import util.ShaEncrypt.TypeEncrypt;

public class EncryptionManager {
	private RsaEncrypt rsa;
	private ShaEncrypt sha;
	private String publicKey;
	private boolean newFile;
	
	public EncryptionManager(int defKeySize, String fileName){
		{
			Config keypairs = new Config("./keys/"+fileName+".json");
			publicKey = keypairs.get("PublicKey", "");
			String privateKey = keypairs.get("PrivateKey", "");
			if(!publicKey.equals("") && !privateKey.equals("")){
				newFile = false;
				try {
					rsa = new RsaEncrypt(publicKey, privateKey);
				} catch (InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException
						| NoSuchPaddingException e) {
					System.out.println("Erreur avec l'initialisation de l'encrypteur RSA");
					e.printStackTrace();
					System.exit(1);
				}
			}else{
				if(defKeySize < 2048){
					System.out.println("La taille de la cl� ne doit pas �tre en dessous de 2048 pour des raisons de s�curit�");
					defKeySize = 2048;
				}
				try {
					newFile = true;
					rsa = new RsaEncrypt(defKeySize);
				} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException e) {
					System.out.println("Erreur avec l'initialisation et la g�n�ration de la cl� de l'encrypteur RSA");
					e.printStackTrace();
					System.exit(1);
				}
				publicKey = rsa.getPublicKey();
				privateKey = rsa.getPrivateKey();
				keypairs.set("PublicKey", publicKey);
				keypairs.set("PrivateKey", privateKey);
			}
		}
		rsa.clearKey();
		try {
			sha = new ShaEncrypt(TypeEncrypt.SHA512);
		} catch (NoSuchAlgorithmException e) {
			System.out.println("Impossible d'initialiser le hachage sha");
			e.printStackTrace();
		}
	}

	public RsaEncrypt getRsa() {
		return rsa;
	}

	public ShaEncrypt getSha() {
		return sha;
	}

	public String getPublicKey() {
		return publicKey;
	}

	public boolean isNewFile() {
		return newFile;
	}
}
