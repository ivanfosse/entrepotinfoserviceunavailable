package util;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/**
 * Classe permettant de hacher un String avec une des variantes de l'algorithme de SHA
 * @author Ivan Fosse
 */
public class ShaEncrypt {
	private MessageDigest sha = null;
	private Charset charset = null;
	
	{
		if(Charset.isSupported("UTF-8"))
			charset = Charset.forName("UTF-8");
		else {
			charset = Charset.defaultCharset();
			System.out.println("Unsuported UTF-8 charged default charset :" + charset.name());
		}
	}
	
	/**
	 * Cr�er une instanciation avec un des algorithme donn�e
	 * @param type l'algorithme choisie
	 * @throws NoSuchAlgorithmException
	 */
	public ShaEncrypt(TypeEncrypt type) throws NoSuchAlgorithmException {
		sha = MessageDigest.getInstance(type.toString());
	}
	
	/**
	 * Cr�er une instanciation avec un des algorithme donn�e et un charset particulier
	 * @param type l'algorithme choisie
	 * @param charset le charset
	 * @throws NoSuchAlgorithmException
	 */
	public ShaEncrypt(TypeEncrypt type, Charset charset) throws NoSuchAlgorithmException {
		this.charset = charset;
		sha = MessageDigest.getInstance(type.toString());
	}
	
	/**
	 * Hache le message (en utf-8 par d�faut si possible) et retourne le r�sultat en Base64
	 * @param le message
	 * @return le r�sultat en Base64
	 */
	public String hash(String message) {
		sha.reset();
		return Base64.getEncoder().encodeToString(sha.digest(message.getBytes(charset)));
	}
	
	/**
	 * Hache le message (en utf-8 par d�faut si possible) et retourne le r�sultat en Base64
	 * @param le message
	 * @param salt le sel utilis�
	 * @return
	 */
	public String hash(String message, String salt) {
		sha.reset();
		sha.update(salt.getBytes());
		return Base64.getEncoder().encodeToString(sha.digest(message.getBytes(charset)));
	}
	
	/**
	 * R�cup�re le charset utilis� pour encrypter les donn�es.
	 */
	public Charset getCharset() {
		return charset;
	}

	/**
	 * D�finie le charset utilis� pour encrypter les donn�es.
	 * @param charset Le charset
	 */
	public void setCharset(Charset charset) {
		this.charset = charset;
	}	
	
	public enum TypeEncrypt {
		SHA512("SHA-512"), 
		SHA256("SHA-256");

		private String name = "";

		private TypeEncrypt(String name) {
			this.name = name;
		}

		public String toString() {
			return name;
		}
	}
}
