package util;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.time.LocalDateTime;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.json.JSONException;
import org.json.JSONObject;

import util.ShaEncrypt.TypeEncrypt;
import util.exception.BadCertificateException;
import util.exception.CertificateExpiredException;
import util.exception.WrongCertificateException;

public class VerifyCertificate {
	public RsaEncrypt rsa;
	public ShaEncrypt sha;
	public String generalCertificate;
	

	public VerifyCertificate(String generalCertificate) throws WrongCertificateException, CertificateExpiredException, BadCertificateException, InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, JSONException, IllegalBlockSizeException, BadPaddingException {
		try {
			sha = new ShaEncrypt(TypeEncrypt.SHA512);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		changeGeneralCertificate(generalCertificate);
	}
	
	public void changeGeneralCertificate(String generalCertificate) throws WrongCertificateException, CertificateExpiredException, BadCertificateException, InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, JSONException, IllegalBlockSizeException, BadPaddingException {
		JSONObject json = new JSONObject(generalCertificate);
		String certStr = json.getString("Certificate");
		JSONObject cert = new JSONObject(certStr);
		String type = cert.getString("Type");
		if(!type.equals("General"))
			throw new WrongCertificateException();
		rsa = new RsaEncrypt(cert.getString("PublicKey"));
		String parmentier = rsa.decryptWithPublic(json.getString("Signature"));
		String hachi = sha.hash(certStr);
		if(!hachi.equals(parmentier))
			throw new BadCertificateException();
		this.generalCertificate = generalCertificate;
	}
	
	
	public void verifyCertificate(String certificate) throws BadCertificateException, CertificateExpiredException, IllegalBlockSizeException, BadPaddingException, JSONException {
		JSONObject json = new JSONObject(certificate);
		String certStr = json.getString("Certificate");
		JSONObject cert = new JSONObject(certStr);
		if(cert.has("Validity")){
			LocalDateTime time = LocalDateTime.parse(cert.getString("Validity"));
			if(time.isBefore(LocalDateTime.now())) 
				throw new CertificateExpiredException();
		}
		String parmentier = rsa.decryptWithPublic(json.getString("Signature"));
		String hachi = sha.hash(certStr);
		if(!hachi.equals(parmentier))
			throw new BadCertificateException();
	}
	
	public void verifyCertificate(String certificate, String... type) throws BadCertificateException, CertificateExpiredException, IllegalBlockSizeException, BadPaddingException, JSONException {
		JSONObject json = new JSONObject(certificate);
		String certStr = json.getString("Certificate");
		JSONObject cert = new JSONObject(certStr);
		boolean continuev = false;
		for (int i = 0; i < type.length; i++) {
			if(cert.getString("Type").equals(type[i])){
				continuev = true;
			}
		}
		if(!continuev)
			throw new BadCertificateException();
		if(cert.has("Validity")){
			LocalDateTime time = LocalDateTime.parse(cert.getString("Validity"));
			if(time.isBefore(LocalDateTime.now())) 
				throw new CertificateExpiredException();
		}
		String parmentier = rsa.decryptWithPublic(json.getString("Signature"));
		String hachi = sha.hash(certStr);
		if(!hachi.equals(parmentier))
			throw new BadCertificateException();
	}
	
	public String getGeneralCertificate() {
		return generalCertificate;
	}

	public static String getPublicKeyFromCertificate(String certificate) throws Exception{
		return new JSONObject(new JSONObject(certificate).getString("Certificate")).getString("PublicKey");
	}
}
