package util.exception;

public class CantGetCertificateException extends Exception{
	public CantGetCertificateException() {
		super("Can't get Server Certificate, unknown server type or already given");
	}
}
