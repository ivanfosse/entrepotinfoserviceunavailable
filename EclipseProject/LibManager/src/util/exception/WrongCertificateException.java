package util.exception;

public class WrongCertificateException extends Exception {
	public WrongCertificateException() {
		super("Wrong Certificate");
	}
}
