package util.exception;

public class CertificateExpiredException extends Exception {
	public CertificateExpiredException() {
		super("Certificate Expired");
	}
}
