package util.exception;

public class BadCertificateException extends Exception {
	public BadCertificateException() {
		super("Bad Certificate wrong signature");
	}
}
