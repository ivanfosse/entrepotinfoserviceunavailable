package util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.json.JSONObject;

public class Config {
	private String filePath;
	private JSONObject node;
	
	public Config() {
		this("./Config.json");
	}
	
	public Config(String path) {
		this.filePath = path;
		try {
			Files.createDirectories(Paths.get(path).getParent());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		node = new JSONObject();
		this.load();
	}
	
	public void save() {
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(new File(filePath)));
			bw.write(node.toString(3));
			bw.flush();
			bw.close();
			bw = null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void load() {
		try {
			String text = new String(Files.readAllBytes(Paths.get(filePath)));
			node = new JSONObject(text);
		} catch (IOException e) {
		}
	}
	
	public String get(String key) {
		if(node.has(key)) {
			return (String) node.get(key);
		}
		return null;
	}
	
	public String get(String key, String defaultValue) {
		if(node.has(key)) {
			return (String) node.get(key);
		}
		node.put(key, defaultValue);
		this.save();
		return defaultValue;
	}
	
	public void set(String key, String value) {
		node.put(key, value);
		this.save();
	}
}
