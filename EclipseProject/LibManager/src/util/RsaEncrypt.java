package util;

import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;


/**
 * Classe qui g�re automatiquement l'encryptage et le d�cryptage avec l'algorithme RSA
 * Les cl�s publique et priv�e sont des String encod� en Base64
 * La cl� publique est une X509EncodedKeySpec
 * La cl� priv�e est une PKCS8EncodedKeySpec
 * @author Ivan Fosse
 */
public class RsaEncrypt {
	private String publicKey;
	private String privateKey;
	private Cipher publicCypherEncrypt;
	private Cipher publicCypherDecrypt;
	private Cipher privateCypherEncrypt;
	private Cipher privateCypherDecrypt;
	private Charset charset = null;
	
	{
		if(Charset.isSupported("UTF-8"))
			charset = Charset.forName("UTF-8");
		else {
			charset = Charset.defaultCharset();
			System.out.println("Unsuported UTF-8 charged default charset :" + charset.name());
		}
	}
	
	/**
	 * Initialise la classe et g�n�re une paire cl�s priv�e et publique avec une taille de 2048 bits
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 */
	public RsaEncrypt() throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException {
		this(2048);
	}
	
	/**
	 * Initialise la classe et g�n�re une paire cl�s priv�e et publique
	 * @param size Taille de la cl� publique et priv�e
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws InvalidKeyException
	 */
	public RsaEncrypt(int size) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException {
		KeyPairGenerator generateurCles = null;
		generateurCles = KeyPairGenerator.getInstance("RSA");
		generateurCles.initialize(size);
		KeyPair paireCles = generateurCles.generateKeyPair();
		publicKey = Base64.getEncoder().encodeToString(paireCles.getPublic().getEncoded());
		privateKey = Base64.getEncoder().encodeToString(paireCles.getPrivate().getEncoded());
		publicCypherEncrypt = Cipher.getInstance("RSA");
		publicCypherEncrypt.init(Cipher.ENCRYPT_MODE, paireCles.getPublic());
		publicCypherDecrypt = Cipher.getInstance("RSA");
		publicCypherDecrypt.init(Cipher.DECRYPT_MODE, paireCles.getPublic());
		privateCypherDecrypt = Cipher.getInstance("RSA");
		privateCypherDecrypt.init(Cipher.DECRYPT_MODE, paireCles.getPrivate());
		privateCypherEncrypt = Cipher.getInstance("RSA");
		privateCypherEncrypt.init(Cipher.ENCRYPT_MODE, paireCles.getPrivate());
	}
	
	/**
	 * Initialise la classe avec une cl� publique seulement
	 * @param publicKey la cl� publique encod� en Base64
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 * @throws NoSuchPaddingException
	 * @throws InvalidKeyException
	 */
	public RsaEncrypt(String publicKey) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException {
		this.publicKey = publicKey;
		this.privateKey = null;
		X509EncodedKeySpec publicspec = new X509EncodedKeySpec(Base64.getDecoder().decode(publicKey));
		KeyFactory kf = KeyFactory.getInstance("RSA");
		PublicKey pKey = kf.generatePublic(publicspec);
		publicCypherEncrypt = Cipher.getInstance("RSA");
		publicCypherEncrypt.init(Cipher.ENCRYPT_MODE, pKey);
		publicCypherDecrypt = Cipher.getInstance("RSA");
		publicCypherDecrypt.init(Cipher.DECRYPT_MODE, pKey);
	}
	
	/**
	 * Initialise la classe avec une cl� publique seulement
	 * @param publicKey la cl� publique encod� en Base64
	 * @param charset le charset utilis� pour encod� suppl�mentairement les messages
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 * @throws NoSuchPaddingException
	 * @throws InvalidKeyException
	 */
	public RsaEncrypt(String publicKey, Charset charset) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException {
		this.charset = charset;
		this.publicKey = publicKey;
		this.privateKey = null;
		X509EncodedKeySpec publicspec = new X509EncodedKeySpec(Base64.getDecoder().decode(publicKey));
		KeyFactory kf = KeyFactory.getInstance("RSA");
		PublicKey pKey = kf.generatePublic(publicspec);
		publicCypherEncrypt = Cipher.getInstance("RSA");
		publicCypherEncrypt.init(Cipher.ENCRYPT_MODE, pKey);
		publicCypherDecrypt = Cipher.getInstance("RSA");
		publicCypherDecrypt.init(Cipher.DECRYPT_MODE, pKey);
	}
	
	/**
	 * Initialise la classe avec une cl� publique et priv�e
	 * @param publicKey la cl� publique encod� en Base64
	 * @param privateKey la cl� priv�e encod� en Base64
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 * @throws NoSuchPaddingException
	 * @throws InvalidKeyException
	 */
	public RsaEncrypt(String publicKey, String privateKey) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException {
		this.publicKey = publicKey;
		this.privateKey = privateKey;
		X509EncodedKeySpec publicspec = new X509EncodedKeySpec(Base64.getDecoder().decode(publicKey));
		PKCS8EncodedKeySpec privatespec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(privateKey));
		KeyFactory kf = KeyFactory.getInstance("RSA");
		PublicKey pKey = kf.generatePublic(publicspec);
		publicCypherEncrypt = Cipher.getInstance("RSA");
		publicCypherEncrypt.init(Cipher.ENCRYPT_MODE, pKey);
		publicCypherDecrypt = Cipher.getInstance("RSA");
		publicCypherDecrypt.init(Cipher.DECRYPT_MODE, pKey);
		PrivateKey prKey = kf.generatePrivate(privatespec);
		privateCypherDecrypt = Cipher.getInstance("RSA");
		privateCypherDecrypt.init(Cipher.DECRYPT_MODE, prKey);
		privateCypherEncrypt = Cipher.getInstance("RSA");
		privateCypherEncrypt.init(Cipher.ENCRYPT_MODE, prKey);
	}
	
	/**
	 * Initialise la classe avec une cl� publique et priv�e
	 * @param publicKey la cl� publique encod� en Base64
	 * @param privateKey la cl� priv�e encod� en Base64
	 * @param charset le charset utilis� pour encod� suppl�mentairement les messages
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 * @throws NoSuchPaddingException
	 * @throws InvalidKeyException
	 */
	public RsaEncrypt(String publicKey, String privateKey, Charset charset) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException {
		this.charset = charset;
		this.publicKey = publicKey;
		this.privateKey = privateKey;
		X509EncodedKeySpec publicspec = new X509EncodedKeySpec(Base64.getDecoder().decode(publicKey));
		PKCS8EncodedKeySpec privatespec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(privateKey));
		KeyFactory kf = KeyFactory.getInstance("RSA");
		PublicKey pKey = kf.generatePublic(publicspec);
		publicCypherEncrypt = Cipher.getInstance("RSA");
		publicCypherEncrypt.init(Cipher.ENCRYPT_MODE, pKey);
		publicCypherDecrypt = Cipher.getInstance("RSA");
		publicCypherDecrypt.init(Cipher.DECRYPT_MODE, pKey);
		PrivateKey prKey = kf.generatePrivate(privatespec);
		privateCypherDecrypt = Cipher.getInstance("RSA");
		privateCypherDecrypt.init(Cipher.DECRYPT_MODE, prKey);
		privateCypherEncrypt = Cipher.getInstance("RSA");
		privateCypherEncrypt.init(Cipher.ENCRYPT_MODE, prKey);
	}
	
	/**
	 * Encryption d'un message � partir de la cl� priv�e en utf-8 (par defaut si possible) et retourne un string encrypt� en base 64
	 * @param message le message � encrypter
	 * @return le message encrypt� en base 64
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 */
	public String encryptWithPrivate(String message) throws IllegalBlockSizeException, BadPaddingException {
		if(privateCypherEncrypt == null) return null;
		byte[] msg = message.getBytes(charset);
		return Base64.getEncoder().encodeToString(privateCypherEncrypt.doFinal(msg));
	}
	
	/**
	 * Encryption d'un message � partir de la cl� public en utf-8 (par defaut si possible) et retourne un string encrypt� en base 64
	 * @param message le message � encrypter
	 * @return le message encrypt� en base 64
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 */
	public String encryptWithPublic(String message) throws IllegalBlockSizeException, BadPaddingException {
		if(publicCypherEncrypt == null) return null;
		byte[] msg = message.getBytes(charset);
		return Base64.getEncoder().encodeToString(publicCypherEncrypt.doFinal(msg));
	}
	
	/**
	 * D�cryption d'un message � partir de la cl� priv�e en utf-8 (par defaut si possible) et retourne le message encod� avec le charset donn�e
	 * @param le message encrypt� en base 64
	 * @return le message encod� avec le charset donn�e
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 */
	public String decryptWithPrivate(String message) throws IllegalBlockSizeException, BadPaddingException {
		if(privateCypherDecrypt == null) return null;
		return new String(privateCypherDecrypt.doFinal(Base64.getDecoder().decode(message)),charset);
	}
	
	/**
	 * D�cryption d'un message � partir de la cl� public en utf-8 (par defaut si possible) et retourne le message encod� avec le charset donn�e
	 * @param le message encrypt� en base 64
	 * @return le message encod� avec le charset donn�e
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 */
	public String decryptWithPublic(String message) throws IllegalBlockSizeException, BadPaddingException {
		if(publicCypherDecrypt == null) return null;
		return new String(publicCypherDecrypt.doFinal(Base64.getDecoder().decode(message)),charset);
	}
	
	/**
	 * R�cup�re la cl� publique si elle est toujours stock�e.
	 */
	public String getPublicKey(){
		return publicKey;
	}
	
	/**
	 * R�cup�re la cl� priv�e si elle est toujours stock�e.
	 */
	public String getPrivateKey(){
		return privateKey;
	}
	
	/**
	 * R�cup�re le charset utilis� pour encrypter les donn�es.
	 */
	public Charset getCharset() {
		return charset;
	}

	/**
	 * D�finie le charset utilis� pour encrypter les donn�es.
	 * @param charset Le charset
	 */
	public void setCharset(Charset charset) {
		this.charset = charset;
	}	
	
	/**
	 * Supprime les cl�s publiques et priv�e de la m�moire rendant les getter inop�rant pour les cl�s (Utile pour la s�curit�)
	 */
	public void clearKey() {
		publicKey = null;
		privateKey = null;
		System.gc();
	}
	
	public static String encryptWithPublicGiven(String message, String publickey) throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		RsaEncrypt rsa = new RsaEncrypt(publickey);
		return rsa.encryptWithPublic(message);
	}
	
	public static String decryptWithPublicGiven(String message, String publickey) throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		RsaEncrypt rsa = new RsaEncrypt(publickey);
		return rsa.decryptWithPublic(message);
	}
}
