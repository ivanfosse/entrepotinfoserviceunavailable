package util;

import java.util.Scanner;
import java.util.SortedMap;
import java.util.TreeMap;

public class Console {
	private SortedMap<String, Command> commands;
	private boolean stop = false;
	public String consoleName;
	
	public Console(){
		consoleName = "Console";
		this.commands = new TreeMap<String, Command>();
		this.addCommand("stop", new Command() {
			@Override
			public void execute(String[] args) {
				System.out.println("Arr�t du programme.");
				stop = true;
			}
		});
		this.addCommand("help", new Command() {
			@Override
			public void execute(String[] args) {
				System.out.println("Liste des commandes :");
				int i = 0;
				int len = commands.keySet().size();
				for (String string : commands.keySet()) {
					System.out.print(string);
					i++;
					if(i != len){
						System.out.print(", ");
					}
				}
				System.out.println();
			}
		});
	}
	
	public Console(String consoleName){
		consoleName = consoleName;
		commands = new TreeMap<String, Command>();
		this.addCommand("stop", new Command() {
			@Override
			public void execute(String[] args) {
				System.out.println("Arr�t du programme.");
				stop = true;
			}
		});
		this.addCommand("help", new Command() {
			@Override
			public void execute(String[] args) {
				System.out.println("Liste des commandes :");
				int i = 0;
				int len = commands.keySet().size();
				for (String string : commands.keySet()) {
					System.out.print(string);
					i++;
					if(i != len){
						System.out.print(", ");
					}
				}
				System.out.println();
			}
		});
	}
	
	public Console(TreeMap<String, Command> commands){
		this.commands = new TreeMap<String, Command>();
		for (String iterable_element : commands.keySet()) {
			this.commands.put(iterable_element, commands.get(iterable_element));
		}
		this.addCommand("stop", new Command() {
			@Override
			public void execute(String[] args) {
				System.out.println("Arr�t du programme.");
				stop = true;
			}
		});
		this.addCommand("help", new Command() {
			@Override
			public void execute(String[] args) {
				System.out.println("Liste des commandes :");
				int i = 0;
				int len = commands.keySet().size();
				for (String string : commands.keySet()) {
					System.out.print(string);
					i++;
					if(i != len){
						System.out.print(", ");
					}
				}
				System.out.println();
			}
		});
	}
	
	public Console(String consoleName, TreeMap<String, Command> commands){
		this.consoleName = consoleName;
		this.commands = new TreeMap<String, Command>();
		for (String iterable_element : commands.keySet()) {
			this.commands.put(iterable_element, commands.get(iterable_element));
		}
		this.addCommand("stop", new Command() {
			@Override
			public void execute(String[] args) {
				System.out.println("Arr�t du programme.");
				stop = true;
			}
		});
		this.addCommand("help", new Command() {
			@Override
			public void execute(String[] args) {
				System.out.println("Liste des commandes :");
				int i = 0;
				int len = commands.keySet().size();
				for (String string : commands.keySet()) {
					System.out.print(string);
					i++;
					if(i != len){
						System.out.print(", ");
					}
				}
				System.out.println();
			}
		});
	}
	
	public void launch(){
		System.out.println("Lancement de la Console :"+consoleName);
		Scanner sc = new Scanner(System.in);
		stop = false;
		while(!stop){
			System.out.print("Commande :");
			String str = sc.nextLine();
			String[] args = str.split(" ");
			str = str.toLowerCase();
			if(args.length == 0)
				continue;
			args[0] = args[0].toLowerCase();
			Command c = commands.get(args[0]);
			if(c == null){
				System.out.println("Unknown command : "+ args[0]);
				continue;}
			c.execute(args);
		}
	}
	
	public void stop(){
		stop = true;
	}
	
	public void addCommand(String name, Command command){
		commands.put(name.toLowerCase(), command);
	}
}
