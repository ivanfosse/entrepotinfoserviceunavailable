package rmiIterface;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RmiGeneralCertificateManager extends Remote {
	public String getGeneralCertificate() throws RemoteException;
}
