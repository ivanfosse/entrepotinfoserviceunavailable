package rmiIterface;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RmiWarehouseCertificateManager extends Remote {
	public String getWarehouseCertificate(String name) throws RemoteException;
}
