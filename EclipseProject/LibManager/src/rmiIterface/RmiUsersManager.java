package rmiIterface;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RmiUsersManager extends Remote {

	/**
	 * Retourne l'id et un salt d'un user ou error si rien
	 * @param certificate
	 * @param args
	 * @return
	 * @throws RemoteException
	 */
	public String getUserId(String certificate, String json) throws RemoteException;
	/**
	 * Retourne les informations de l'user si l'id et le hashedpassword est bon
	 * @param certificate
	 * @param args
	 * @return
	 * @throws RemoteException
	 */
	public String getInformation(String certificate, String json) throws RemoteException;
	/**
	 * Get Certificate
	 * @return ServerCertificate of UserManager
	 */
	public String getCertificate() throws RemoteException;
	
}
