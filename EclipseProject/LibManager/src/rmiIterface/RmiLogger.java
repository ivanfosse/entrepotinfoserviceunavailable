package rmiIterface;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RmiLogger extends Remote {
	public void addLog(String str, String typeServer) throws RemoteException;
}
