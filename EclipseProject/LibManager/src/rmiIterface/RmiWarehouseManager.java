package rmiIterface;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RmiWarehouseManager extends Remote {
	public String executeCommand(String certif, String command) throws RemoteException;
	public String getCertificate() throws RemoteException;
}
