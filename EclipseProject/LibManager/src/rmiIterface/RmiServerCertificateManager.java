package rmiIterface;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RmiServerCertificateManager extends Remote {
	public String getServerCertificate(String serverName) throws RemoteException;
}
